@extends('base')

@section('content')
<div class="container">
    <div class="row">
        <div class="d-sm-none d-md-block col-md-3 col-lg-3 col-xl-3">
            <ul class="sidebar-menu">
                <li><a href="#">Мои грузы</a></li>
                <li><a href="#">Мой транспорт</a></li>
            </ul>
        </div>
    </div>
</div>
@endsection