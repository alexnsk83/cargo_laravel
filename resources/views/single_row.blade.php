<!DOCTYPE html>
<html lang="ru">
    <head>
        @include('blocks.head')
    </head>
    <body>
        @include('blocks._header')
        <div class="container">
            <div class="row">
                @section('content')
                @show
            </div>
        </div>
        @include('blocks.footer')
        @include('blocks.copyright')
        @include('blocks.scripts')

    </body>
</html>