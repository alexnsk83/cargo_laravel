@extends('admin.base')

@section('content')
    <section class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <a href="{{ route('admin.post.add') }}" class="btn btn-primary margin-top">{{ trans('site.button.post_new') }}</a>
                @if ($trashed)
                    <a href="{{ route('admin.post.all') }}" class="btn btn-primary margin-top">{{ trans('site.button.post_active') }}</a>
                @else
                    <a href="{{ route('admin.post.trashed') }}" class="btn btn-primary margin-top">{{ trans('site.button.post_trashed') }}</a>
                @endif
            </div>
        </div>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                @if (Session::has('message'))
                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                @endif
                @foreach($posts as $post)
                    <div class="box">
                        <div class="row">
                            <div class="col-xs-12 col-sm-3">
                                @if ($post['image'])
                                    <img class="wp-post-image" src="{{ $post['image'] }}" alt="Blog image">
                                @endif
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <h3>{{ $post['caption'] }}</h3>
                                <p>{!! json_decode($post['announce']) !!}</p>
                                <hr>
                                <a class="admin-ctrl-btn admin-btn fa fa-pencil-square-o" title="{{ trans('site.button.post_edit') }}"
                                   href="{{ route('admin.post.edit', ['id' => $post['id']]) }}"></a>
                                @if ($post->trashed())
                                    <a class="admin-ctrl-btn admin-btn fa fa-magic" title="{{ trans('site.button.post_recover') }}"
                                       href="{{ route('admin.post.recover', ['id' => $post['id']]) }}"></a>
                                @else
                                    <a class="admin-ctrl-btn admin-btn fa fa-trash-o" title="{{ trans('site.button.post_delete') }}"
                                       href="{{ route('admin.post.delete', ['id' => $post['id']]) }}"
                                       onclick="return confirm('{{ trans('site.dialog.delete_post') }}')"></a>
                                @endif
                                <span class="admin-btn">
                                @if ($post['is_visible'])
                                        <span class="published">{{ trans('messages.published') }}</span>
                                    @endif
                            </span>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection