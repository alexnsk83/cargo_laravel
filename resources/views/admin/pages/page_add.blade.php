@extends('admin.base')

@section('content')
    <div class=" container-fluid boxed  push-down-60">
        <div class="post-content">
            <h1>{{ trans('site.title.page_add') }}</h1>
            <form enctype="multipart/form-data" method="post" action="{{ route('admin.page.addPost') }}">
                {{ csrf_field() }}
                @if ($errors->has('name'))
                    <br><span class="text-danger">{{ $errors->get('name')[0] }} </span><br>
                @endif
                <label for="name"><span>Название пункта</span></label>
                <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">
                <br>

                <label for="module"><span>Модуль</span></label>
                <select class="form-control" id="module" name="module">
                    @foreach ($modules as $module)
                    <option value="{{ $module['id'] }}">{{ $module['name'] }}</option>
                    @endforeach
                </select>
                <br>
                <label for="sort_order"><span>Порядок сортировки</span></label>
                <input type="text" class="form-control" id="sort_order" name="sort_order" value="{{ old('sort_order') ? old('sort_order') : '1' }}"><br>
                <label class="checkbox-inline"><input type="checkbox" name="menu_1" value="1" checked>Меню 1</label>
                <label class="checkbox-inline"><input type="checkbox" name="menu_2" value="1" checked>Меню 2</label>
                <br><br>

                <div id="for_pages">
                    @if ($errors->has('slug'))
                        <br><span class="text-danger">{{ $errors->get('slug')[0] }} </span><br>
                    @endif
                    <label for="slug"><span>Slug</span></label>
                    <input type="text" id="slug" name="slug" class="form-control">
                    <br>

                    <label for="text_content"><span>Текст</span></label><br>
                    <textarea id="text_content" name="text" value="{{ old('text') }}"></textarea>
                    <script src="\vendor\unisharp\laravel-ckeditor\ckeditor.js"></script>
                    <script>
                        CKEDITOR.replace( 'text_content', {
                            filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
                            filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token={{csrf_token()}}',
                            filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
                            filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token={{csrf_token()}}'
                        } );
                    </script>
                    <br>
                </div>

                <label for="keywords"><span>{{ trans('site.form.keywords') }}</span></label><br>
                <input type="text" class="form-control" id="keywords" name="keywords" value="{{ old('keywords') }}"><br><br>

                <label for="description"><span>{{ trans('site.form.description') }}</span></label><br>
                <input type="text" class="form-control" id="description" name="description" value="{{ old('description') }}"><br><br>

                <input type="submit" value="{{ trans('site.button.save') }}" class="btn btn-primary">
            </form><br>
            @if (Session::has('message'))
                <div class="alert alert-success">{{ Session::get('message') }}</div>
            @endif
        </div>
    </div>
@endsection