@extends('admin.base')

@section('content')
    <div class="container-fluid boxed  push-down-60">
        <div class="post-content">
            <h1>{{ trans('site.form.gallery_new') }}</h1>
            <form enctype="multipart/form-data" method="post" class="form-group">
                {{ csrf_field() }}

                <label for="name">{{ trans('site.form.gallery_name') }}</label>
                @if ($errors->has('name'))
                    <span class="text-danger">{{ $errors->get('name')[0] }}</span>
                @endif
                <input type="text" id="name" name="name" class="form-control" value="{{ old('name') }}"><br>

                <label for="description">{{ trans('site.form.gallery_description') }}</label>
                @if ($errors->has('description'))
                    <span class="text-danger">{{ $errors->get('description')[0] }}</span>
                @endif
                <input type="text" id="description" name="description" class="form-control" value="{{ old('description') }}"><br>

                <label for="slug"><span>Slug</span></label>
                @if ($errors->has('slug'))
                    <br><span class="text-danger">{{ $errors->get('slug')[0] }} </span><br>
                @endif
                <input type="text" id="slug" name="slug" class="form-control" value="{{ old('slug') }}">
                <br>

                <label for="cover_image">{{ trans('site.form.gallery_cover') }}</label>
                @if ($errors->has('cover_image'))
                    <span class="text-danger">{{ $errors->get('cover_image')[0] }}</span>
                @endif
                <input type="file" id="cover_image" name="cover_image"><br>

                <label for="content_images">{{ trans('site.form.content_images') }}</label><span> ({{ trans('site.form.multiple_load') }})</span>
                @if ($errors->has('content_images'))
                    <span class="text-danger">{{ $errors->get('content_images')[0] }}</span>
                @endif
                <input type="file" id="content_images" multiple name='content_images[]'><br>

                <input type="submit" value="{{ trans('site.button.save') }}"  class="btn btn-primary">
            </form><br>
            @if (Session::has('message'))
                <div class="alert alert-success">{{ Session::get('message') }}</div>
            @endif
        </div>
    </div>
@endsection