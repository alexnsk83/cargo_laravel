@extends('admin.base')

@section('content')
    <div class=" container-fluid boxed  push-down-60">
        <div class="post-content">
            <h1>Изменение пункта меню</h1>
            <form enctype="multipart/form-data" method="post" action="{{ route('admin.page.edit', ['id' => $page['id']]) }}">
                {{ csrf_field() }}
                @if ($errors->has('name'))
                    <span class="text-danger">{{ $errors->get('name')[0] }} </span><br>
                @endif
                <label for="name"><span>Название пункта</span></label>
                <input type="text" class="form-control" id="name" name="name" value="{{ $page['name'] }}">
                <br>
                @if ($errors->has('sort_order'))
                    <span class="text-danger">{{ $errors->get('sort_order')[0] }} </span><br>
                @endif
                <label for="sort_order"><span>Порядок сортировки</span></label>
                <input type="text" class="form-control" id="sort_order" name="sort_order"
                       value="{{ old('sort_order') ? old('sort_order') : $page['sort_order'] }}"><br>
                <label class="checkbox-inline"><input type="checkbox" name="menu_1"
                       value="1" {{ $page['menu_1'] ? 'checked' : '' }}>Меню 1</label>
                <label class="checkbox-inline"><input type="checkbox" name="menu_2"
                       value="1" {{ $page['menu_2'] ? 'checked' : '' }}>Меню 2</label>
                <br><br>

                @if ($page->module_id === 1)
                    <div id="for_pages">
                        @if ($errors->has('slug'))
                            <span class="text-danger">{{ $errors->get('slug')[0] }} </span><br>
                        @endif
                        <label for="slug"><span>Slug</span></label>
                        <input type="text" id="slug" name="slug" class="form-control"
                           @if (isset ($page['slug']))
                               value="{{ $page['slug'] }}"
                           @else
                               value="{{ old('slug') }}"
                           @endif><br>
                        <label for="text_content"><span>Текст</span></label><br>
                        <textarea id="text_content" name="text">
                            @if (isset ($page['text']))
                                {!! $page['text'] !!}
                            @endif
                        </textarea>
                        <script src="\vendor\unisharp\laravel-ckeditor\ckeditor.js"></script>
                        <script>
                            CKEDITOR.replace( 'text_content', {
                                filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
                                filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token={{csrf_token()}}',
                                filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
                                filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token={{csrf_token()}}'
                            } );
                        </script>
                        <br>
                    </div>
                @endif

                <label for="keywords"><span>{{ trans('site.form.keywords') }}</span></label><br>
                <input type="text" class="form-control" id="keywords" name="keywords" value="{{ $page['keywords'] }}"><br>

                <label for="description"><span>{{ trans('site.form.description') }}</span></label><br>
                <input type="text" class="form-control" id="description" name="description" value="{{ $page['description'] }}"><br>

                <input type="submit" value="{{ trans('site.button.save') }}" class="btn btn-primary">
            </form><br>
            @if (Session::has('message'))
                <div class="alert alert-success">{{ Session::get('message') }}</div>
            @endif
        </div>
    </div>
@endsection