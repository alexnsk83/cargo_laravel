@extends('admin.base')

@section('content')
    <div class="container-fluid boxed  push-down-60">
        <div class="post-content">
            <h1>{{ trans('site.form.article_edit') }}</h1>
            <form enctype="multipart/form-data" method="post">
                {{ csrf_field() }}
                <span>{{ trans('site.form.article_name') }}</span>
                @if ($errors->has('caption'))
                    <span class="text-danger">{{ $errors->get('caption')[0] }} </span>
                @endif<br>
                <input type="text" class="form-control" name="caption"
                @if (isset($post['caption']))
                    value="{{ $post['caption'] }}"
                @else
                    value="{{ old('caption') }}"
                @endif><br>

                <span>{{ trans('site.form.article_detail') }}</span>
                @if ($errors->has('detail'))
                    <span class="text-danger">{{ $errors->get('detail')[0] }} </span>
                @endif<br>
                <textarea name="detail" class="form-control" rows="15">@if (isset($post['detail'])){{ trim($post['detail']) }}@endif</textarea><br>
                <script src="\vendor\unisharp\laravel-ckeditor\ckeditor.js"></script>
                <script>
                    CKEDITOR.replace( 'detail', {
                        filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
                        filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token={{csrf_token()}}',
                        filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
                        filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token={{csrf_token()}}'
                    } );
                </script>

                @if (isset($post['image']))
                    <img src="{{ $post['image'] }}" style="max-height: 100px; max-width: 200px"><br>
                @endif

                <span>{{ trans('site.form.article_photo') }}</span>
                @if ($errors->has('iamge'))
                    <span class="text-danger">{{ $errors->get('image')[0] }} </span>
                @endif<br>
                <input type="file" name="image"><br>

                <input type="checkbox"
                       @if (isset($post['show_comments']))
                       checked
                       @endif
                       id="show_comments" name="show_comments" value="1"><label for="show_comments">{{ trans('site.form.show_comments') }}</label><br>

                <input type="checkbox"
                       @if (isset($post['commentable']))
                       checked
                       @endif
                       id="commentable" name="commentable" value="1"><label for="commentable">{{ trans('site.form.commentable') }}</label><br>

                <input type="checkbox"
                       @if (isset($post['is_visible']))
                       checked
                       @endif
                       id="is_visible" name="is_visible" value="1"><label for="is_visible">{{ trans('site.form.publish') }}</label><br>

                <input type="submit" value="{{ trans('site.button.save') }}"  class="btn btn-primary">
                @if (isset($post['deleted_at']))
                    <a class="btn btn-primary" href="{{ route('admin.post.trashed') }}">{{ trans('site.button.back') }}</a>
                @else
                    <a class="btn btn-primary" href="{{ route('admin.post.all') }}">{{ trans('site.button.back') }}</a>
                @endif
            </form><br>
            @if (Session::has('message'))
                <div class="alert alert-success">{{ Session::get('message') }}</div>
            @endif
        </div>
    </div>
@endsection