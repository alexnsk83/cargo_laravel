@extends('admin.base')

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-lg-8 col-md-10">
                @if (Session::has('message'))
                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                @endif
                <div class="box">
                    <div class="box-body">
                        <form method="post">
                            {{ csrf_field() }}
                            <table class="table table-condensed table-responsive">
                                <thread>
                                    <tr>
                                        <th>{{ trans('site.table.avatar') }}</th>
                                        <th>{{ trans('site.table.username') }}</th>
                                        <th>{{ trans('site.table.email') }}</th>
                                        <th>{{ trans('site.table.created_at') }}</th>
                                        <th>{{ trans('site.table.status') }}</th>
                                    </tr>
                                </thread>
                                <tbody>
                                    <tr>
                                        <td>
                                            <img src="{{ $user['avatar'] }}" width="75px">
                                        </td>
                                        <td>{{ $user['name'] }}</td>
                                        <td>{{ $user['email'] }}</td>
                                        <td>{{ $user['created_at'] }}</td>
                                        <td>
                                            @if ($user['id'] > 1)
                                            <select name="status">
                                                @foreach($statuses as $status)
                                                    <option value="{{ $status['id'] }}" {{ $status['id'] == $user['status'] ? 'selected' : '' }} >{{ $status['status'] }}</option>
                                                @endforeach
                                            </select>
                                            @endif
                                            <input type="hidden" name="user_id" value="{{ $user['id'] }}">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <input type="submit" class="btn btn-success" value="{{ trans('site.button.save') }}">
                            @if ($user['id'] > 1)
                            <a onclick="return confirm('{{ trans('site.dialog.delete_user') }}')" href="{{ route('admin.user.delete', $user['id']) }}" class="btn btn-danger">{{ trans('site.button.delete') }}</a>
                            @endif
                            <a href="{{ route('admin.user.all') }}" class="btn btn-danger">{{ trans('site.button.cancel') }}</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection