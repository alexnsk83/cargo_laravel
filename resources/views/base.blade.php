<!DOCTYPE html>
<html lang="ru">
    <head>
        @include('blocks.head')
    </head>
    <body>
        <div class="wrapper">
        @include('blocks.header')
        <div class="content mt-1">
            @section('content')
            @show
        </div>
        @include('blocks.footer')
        @include('blocks.scripts')
        </div>
    </body>
</html>