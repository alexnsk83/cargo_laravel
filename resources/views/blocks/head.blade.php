<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="keywords" content="{{ $keywords or "" }}">
<meta name="description" content="{{ $description or "" }}">
<link rel="shortcut icon" href="{{ URL::to('assets/images/favicon.png') }}">
<meta name="theme-color" content="#185f7d">
<title>{{ $title or 'Title' }}</title>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="{{ URL::to('assets/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ URL::to('assets/css/main.css') }}">
<link rel="stylesheet" href="{{ URL::to('assets/css/lightbox.min.css') }}">
<link rel="stylesheet" href="{{ URL::to('assets/css/jquery-ui.min.css') }}">
<!-- jQuery 3.2.1 -->
<script src="/assets/admin/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI -->
<script src="/assets/admin/plugins/jQueryUI/jquery-ui.min.js"></script>
