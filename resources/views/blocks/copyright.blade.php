<div class="copyrights">
    <div class="container">
        <div class="row">
            <div class="float-left">
                <a href="https://vk.com/id8785339" target="_blank" class="float-right" style="font-size: .8em">
                    Hojuela-CMS by Alexey Balobanov © Copyright {{ date('Y') }}
                </a>
            </div>
        </div>
    </div>
</div>
