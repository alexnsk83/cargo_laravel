<header class="header">
    <div class="container">
        <div class="row">
            <div class="">
                <nav class="navbar navbar-default pull-left" role="navigation">
                    <ul class="navigation pull-left">
                        @if (Auth::check())
                            <div class="greeting">
                                <img class="user-panel" src="{{ Auth::user()->avatar }}">{{ Auth::user()->name }}</div>
                        @endif

                        @if (Auth::check() && Auth::user()->status === 1)
                            <li><a href="{{ route('admin.home') }}">{{ trans('site.button.admin') }}</a></li>
                        @endif
                        @if (Auth::check())
                            <li><a href="{{ route('cabinet.home') }}">{{ trans('site.button.cabinet') }}</a></li>
                            <li><a href="{{ route('site.auth.logout') }}">{{ trans('site.button.logout') }}</a></li>
                        @else
                            <li><a href="{{ route('site.auth.login') }}">{{ trans('site.button.login') }}</a></li>
                        @endif
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</header>
<div class="container logo-block">
    <div class="row">
        <div class="col-xs-12 col-xl-12">
            <div class="logo">
                <div class="header-text">
                    <a href="{{ route('site.home') }}">
                        <h1 class="header-title">Единая транспортно-логистическая сеть</h1>
                        <h2 class="header-tagline">Информационный портал</h2>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="sticky-placeholder"></div>
<div class="second-menu" id="sticky">
    <div class="container">
        <div class="row">
            <nav class="navbar navbar-menu navbar-default" role="navigation">
                <ul class="navigation">
                @if (count($menu_1) > 0)
                    @foreach ($menu_1 as $item) <!-- Получаем из AppServiceProvider -->
                        <li class="col-xs-12">
                            @if ($item->module_id === 1)
                                <a href="{{ route($item->module->url, $item['slug']) }}">{{ $item['name'] }}</a>
                            @else
                                <a href="{{ route($item->module->url) }}">{{ $item['name'] }}</a>
                            @endif
                        </li>
                        @endforeach
                    @else
                        @if (Auth::check() && Auth::user()->status === 1)
                            <li class="col-xs-12">
                                <h5>Не создано ни одного пункта меню</h5>
                            </li>
                        @endif
                    @endif
                </ul>
            </nav>
        </div>
    </div>
</div>

