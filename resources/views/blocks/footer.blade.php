<footer class="footer">
    <div class="container">
        <div class="row">
            <nav class="navbar navbar-default pull-left" role="navigation">
                <ul class="navigation pull-left">
                    @if (Auth::check() && Auth::user()->status === 1)
                        <li><a href="{{ route('admin.home') }}">{{ trans('site.button.admin') }}</a></li>
                    @endif
                    @if (Auth::check())
                        <li><a href="#">{{ trans('site.button.cabinet') }}</a></li>
                        <li><a href="{{ route('site.auth.logout') }}">{{ trans('site.button.logout') }}</a></li>
                    @else
                        <li><a href="{{ route('site.auth.login') }}">{{ trans('site.button.login') }}</a></li>
                    @endif
                </ul>
            </nav>
        </div>
    </div>
    @include('blocks.copyright')
</footer>