<form method="post" action="{{ route('site.cargo.search') }}" class="p-2 mt-1 form-border card-shadow">
    {{ csrf_field() }}
    <h2 class="red-color">Поиск</h2>
    <div class="row">
        <div class="col-xl-6 col-lg-6 col-md-6 col-xs-12 pr-xl-0 pr-lg-0 pr-md-0">
            <div class="form-group">
                <label for="departure_area">Место загрузки</label>
                <select class="form-control select-tiny" name="departure_area" id="departure_area">
                    <option value="" class="form-control">Регион не выбран</option>
                    @foreach($departure_areas as $departure_area)
                        <option class="form-control" value="{{ $departure_area['AOGUID'] }}"
                                {{ (isset($input['departure_area']) && $input['departure_area'] == $departure_area['AOGUID']) ? 'selected' : '' }}>{{ $departure_area['FORMALNAME'] }} {{ $departure_area['SHORTNAME'] }}</option>
                    @endforeach
                </select>
                <select class="form-control select-tiny" name="departure_city" id="departure_city">
                    @if(isset($input['departure_city']))
                        <option value="{{ $input['departure_city'] }}"></option>
                    @endif
                </select>
            </div>
        </div>
        <div class="col-xl-6 col-lg-6 col-md-6 col-xs-12 pl-xl-0 pl-lg-0 pl-md-0">
            <div class="form-group">
                <label for="destination_area">Место выгрузки</label>
                <select class="form-control select-tiny" name="destination_area" id="destination_area">
                    <option value="" class="form-control">Регион не выбран</option>
                    @foreach($destination_areas as $destination_area)
                        <option class="form-control" value="{{ $destination_area['AOGUID'] }}"
                                {{ (isset($input['destination_area']) && $input['destination_area'] == $destination_area['AOGUID']) ? 'selected' : '' }}>{{ $destination_area['FORMALNAME'] }} {{ $destination_area['SHORTNAME'] }}</option>
                    @endforeach
                </select>
                <select class="form-control select-tiny" name="destination_city" id="destination_city">
                    @if(isset($input['destination_city']))
                        <option value="{{ $input['destination_city'] }}"></option>
                    @endif
                </select>
            </div>
        </div>

    </div>

    <div class="form-group form-inline">
        <input type="text" value="{{ isset($input['weight_min']) ? $input['weight_min'] : '' }}" class="form-control col-xl-6 col-lg-6 col-md-6 col-sm-6" name="weight_min" placeholder="Вес мин. (т.)">
        <input type="text" value="{{ isset($input['weight_max']) ? $input['weight_max'] : '' }}" class="form-control col-xl-6 col-lg-6 col-md-6 col-sm-6" name="weight_max" placeholder="Вес макс. (т.)">
    </div>

    <div class="form-group form-inline">
        <input type="text" value="{{ isset($input['volume_min']) ? $input['volume_min'] : '' }}" class="form-control col-xl-6 col-lg-6 col-md-6 col-sm-6" name="volume_min" placeholder="Объём мин. (куб м.)">
        <input type="text" value="{{ isset($input['volume_max']) ? $input['volume_max'] : '' }}" class="form-control col-xl-6 col-lg-6 col-md-6 col-sm-6" name="volume_max" placeholder="Объём макс. (куб м.)">
    </div>

    <h5>Габариты</h5>
    <div class="form-group form-inline">
        <input type="text" value="{{ isset($input['length_min']) ? $input['length_min'] : '' }}" class="form-control col-xl-6 col-lg-6 col-md-6 col-sm-6" name="lenght_min" placeholder="Длина, мин. (м.)">
        <input type="text" value="{{ isset($input['length_max']) ? $input['length_max'] : '' }}" class="form-control col-xl-6 col-lg-6 col-md-6 col-sm-6" name="lenght_max" placeholder="Длина, макс. (м.)">
        <input type="text" value="{{ isset($input['width_min']) ? $input['width_min'] : '' }}" class="form-control col-xl-6 col-lg-6 col-md-6 col-sm-6" name="width_min" placeholder="Ширина, мин. (м.)">
        <input type="text" value="{{ isset($input['width_max']) ? $input['width_max'] : '' }}" class="form-control col-xl-6 col-lg-6 col-md-6 col-sm-6" name="width_max" placeholder="Ширина, макс. (м.)">
        <input type="text" value="{{ isset($input['height_min']) ? $input['height_min'] : '' }}" class="form-control col-xl-6 col-lg-6 col-md-6 col-sm-6" name="height_min" placeholder="Высота, мин. (м.)">
        <input type="text" value="{{ isset($input['height_max']) ? $input['height_max'] : '' }}" class="form-control col-xl-6 col-lg-6 col-md-6 col-sm-6" name="height_max" placeholder="Высота, макс. (м.)">
    </div>

    <h5>Дата отправления</h5>
    <div class="form-group form-inline">
        <input type="date" value="{{ isset($input['date_start_min']) ? $input['date_start_min'] : '' }}" class="form-control col-xl-6 col-lg-6 col-md-6 col-sm-6" name="date_start_min" placeholder="Дата отправления">
        <input type="date" value="{{ isset($input['date_start_max']) ? $input['date_start_max'] : '' }}" class="form-control col-xl-6 col-lg-6 col-md-6 col-sm-6" name="date_start_max">
    </div>

    <h5>Дата прибытия</h5>
    <div class="form-group form-inline">
        <input type="date" value="{{ isset($input['date_end_min']) ? $input['date_end_min'] : '' }}" class="form-control col-xl-6 col-lg-6 col-md-6 col-sm-6" name="date_end_min" placeholder="Дата прибытия">
        <input type="date" value="{{ isset($input['date_end_max']) ? $input['date_end_max'] : '' }}" class="form-control col-xl-6 col-lg-6 col-md-6 col-sm-6" name="date_end_max">
    </div>
    <div class="row">
        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
            <div class="form-group">
                <h5>Тип кузова</h5>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="capony" value="1" {{ isset($input['capony']) ? 'checked' : '' }}>Тент
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="frige" value="1" {{ isset($input['frige']) ? 'checked' : '' }}>Рефриджератор
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="isotherm" value="1" {{ isset($input['isotherm']) ? 'checked' : '' }}>Изотерм
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="metal" value="1" {{ isset($input['metal']) ? 'checked' : '' }}>Цельнометаллический
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="flatbed" value="1" {{ isset($input['flatbed']) ? 'checked' : '' }}>Бортовой
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="tipper" value="1" {{ isset($input['tipper']) ? 'checked' : '' }}>Самосвал
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="container" value="1" {{ isset($input['container']) ? 'checked' : '' }}>Контейнеровоз
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="trawl" value="1" {{ isset($input['trawl']) ? 'checked' : '' }}>Трал
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="tank" value="1" {{ isset($input['tank']) ? 'checked' : '' }}>Цистерна
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="grain" value="1" {{ isset($input['grain']) ? 'checked' : '' }}>Зерновоз
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="livestock" value="1" {{ isset($input['livestock']) ? 'checked' : '' }}>Скотовоз
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="car" value="1" {{ isset($input['car']) ? 'checked' : '' }}>Автовоз
                    </label>
                </div>
            </div>
        </div>
        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
            <div class="form-group">
                <h5>Тип загрузки</h5>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="rear" value="1" {{ isset($input['rear']) ? 'checked' : '' }}>Задняя
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="top" value="1" {{ isset($input['top']) ? 'checked' : '' }}>Верхняя
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="side" value="1" {{ isset($input['side']) ? 'checked' : '' }}>Боковая
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="rear-side" value="1" {{ isset($input['rear-side']) ? 'checked' : '' }}>Зад., бок.
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="full" value="1" {{ isset($input['full']) ? 'checked' : '' }}>Полная растентовка
                    </label>
                </div>

                <div class="form-group">
                    <h5>Оплата</h5>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="prepay" value="1" {{ isset($input['prepay']) ? 'checked' : '' }}>С предоплатой
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="nostake" value="1" {{ isset($input['nostake']) ? 'checked' : '' }}>Без ставки
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="cash" value="1" {{ isset($input['cash']) ? 'checked' : '' }}>Наличная оплата
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="nonds" value="1" {{ isset($input['nonds']) ? 'checked' : '' }}>Безналичная без НДС
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="nds" value="1" {{ isset($input['nds']) ? 'checked' : '' }}>Безналичная с НДС
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="card" value="1" {{ isset($input['card']) ? 'checked' : '' }}>На карту
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="afterloads">Догруз</label>
        <select class="form-control select-tiny" name="afterloads" id="afterloads">
            @foreach($afterloads as $afterload)
                <option class="form-control" value="{{ $afterload['id'] }}"
                {{ (isset($input['afterloads']) && $input['afterloads'] == $afterload['id']) ? 'selected' : '' }}>{{ $afterload['name'] }}</option>
            @endforeach
        </select>
        <label for="extra">Дополнительные параметры</label>
        <select class="form-control select-tiny" name="extra" id="extra">
            @foreach($extraparams as $extraparam)
                <option class="form-control" value="{{ $extraparam['id'] }}"
                {{ (isset($input['extra']) && $input['extra'] == $extraparam['id']) ? 'selected' : '' }}>{{ $extraparam['name'] }}</option>
            @endforeach
        </select>
    </div>
    <input class="button" type="submit" name="cargo" value="Найти груз">
    <input class="button" type="submit" name="truck" value="Найти транспорт">
</form>