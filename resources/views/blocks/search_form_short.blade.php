<form method="post" action="{{ route('site.cargo.search') }}" class="p-2 mt-1 form-border card-shadow">
    {{ csrf_field() }}
    <h2 class="red-color">Поиск</h2>
    <div class="row">
        <div class="col-xl-6 col-lg-6 col-md-6 col-xs-12 pr-xl-0 pr-lg-0 pr-md-0">
            <div class="form-group">
                <label for="departure_area">Место загрузки</label>
                <select class="form-control select-tiny" name="departure_area" id="departure_area">
                    <option value="" class="form-control">Регион не выбран</option>
                    @foreach($departure_areas as $departure_area)
                        <option class="form-control" value="{{ $departure_area['AOGUID'] }}"
                                {{ (isset($input['departure_area']) && $input['departure_area'] == $departure_area['AOGUID']) ? 'selected' : '' }}>{{ $departure_area['FORMALNAME'] }} {{ $departure_area['SHORTNAME'] }}</option>
                    @endforeach
                </select>
                <select class="form-control select-tiny" name="departure_city" id="departure_city">
                    @if(isset($input['departure_city']))
                        <option value="{{ $input['departure_city'] }}"></option>
                    @endif
                </select>
            </div>
        </div>
        <div class="col-xl-6 col-lg-6 col-md-6 col-xs-12 pl-xl-0 pl-lg-0 pl-md-0">
            <div class="form-group">
                <label for="destination_area">Место выгрузки</label>
                <select class="form-control select-tiny" name="destination_area" id="destination_area">
                    <option value="" class="form-control">Регион не выбран</option>
                    @foreach($destination_areas as $destination_area)
                        <option class="form-control" value="{{ $destination_area['AOGUID'] }}"
                                {{ (isset($input['destination_area']) && $input['destination_area'] == $destination_area['AOGUID']) ? 'selected' : '' }}>{{ $destination_area['FORMALNAME'] }} {{ $destination_area['SHORTNAME'] }}</option>
                    @endforeach
                </select>
                <select class="form-control select-tiny" name="destination_city" id="destination_city">
                    @if(isset($input['destination_city']))
                        <option value="{{ $input['destination_city'] }}"></option>
                    @endif
                </select>
            </div>
        </div>

    </div>

    <div class="form-group form-inline">
        <input type="text" value="{{ isset($input['weight_min']) ? $input['weight_min'] : '' }}" class="form-control col-xl-6 col-lg-6 col-md-6 col-sm-6" name="weight_min" placeholder="Вес мин. (т.)">
        <input type="text" value="{{ old('weight_max') }}" class="form-control col-xl-6 col-lg-6 col-md-6 col-sm-6" name="weight_max" placeholder="Вес макс. (т.)">
    </div>

    <div class="form-group form-inline">
        <input type="text" value="{{ isset($input['volume_min']) ? $input['volume_min'] : '' }}" class="form-control col-xl-6 col-lg-6 col-md-6 col-sm-6" name="volume_min" placeholder="Объём мин. (куб м.)">
        <input type="text" value="{{ isset($input['volume_max']) ? $input['volume_max'] : '' }}" class="form-control col-xl-6 col-lg-6 col-md-6 col-sm-6" name="volume_max" placeholder="Объём макс. (куб м.)">
    </div>

    <h5>Дата отправления</h5>
    <div class="form-group form-inline">
        <input type="date" value="{{ isset($input['date_start_min']) ? $input['date_start_min'] : '' }}" class="form-control col-xl-6 col-lg-6 col-md-6 col-sm-6" name="date_start_min" placeholder="Дата отправления">
        <input type="date" value="{{ isset($input['date_start_max']) ? $input['date_start_max'] : '' }}" class="form-control col-xl-6 col-lg-6 col-md-6 col-sm-6" name="date_start_max">
    </div>

    <h5>Дата прибытия</h5>
    <div class="form-group form-inline">
        <input type="date" value="{{ isset($input['date_end_min']) ? $input['date_end_min'] : '' }}" class="form-control col-xl-6 col-lg-6 col-md-6 col-sm-6" name="date_end_min" placeholder="Дата прибытия">
        <input type="date" value="{{ isset($input['date_end_max']) ? $input['date_end_max'] : '' }}" class="form-control col-xl-6 col-lg-6 col-md-6 col-sm-6" name="date_end_max">
    </div>
    <div class="form-group">
        <h5>Тип кузова</h5>
        <div class="row">
            <div class="col-xl-3">
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="capony" value="1">Тент
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="frige" value="1">Рефриджератор
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="isotherm" value="1">Изотерм
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="metal" value="1">Цельнометаллический
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="flatbed" value="1">Бортовой
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="tipper" value="1">Самосвал
                    </label>
                </div>
            </div>
            <div class="col-xl-3">
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="container" value="1">Контейнеровоз
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="trawl" value="1">Трал
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="tank" value="1">Цистерна
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="grain" value="1">Зерновоз
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="livestock" value="1">Скотовоз
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="car" value="1">Автовоз
                    </label>
                </div>
            </div>
        </div>
    </div>
    <input class="button" type="submit" name="cargo" value="Найти груз">
    <input class="button" type="submit" name="truck" value="Найти транспорт">
</form>