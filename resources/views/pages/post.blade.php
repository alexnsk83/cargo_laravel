@extends('single_row')

@section('content')
<div class="boxed  push-down-60">
    <div class="meta col-xs-10  col-xs-offset-1  col-md-8  col-md-offset-2  push-down-60">
        @if (!empty($post['image']))
            <img class="wp-post-image" src="{{$post['image']}}" alt="Blog image" height="493px">
        @endif
        <div class="row">
            <div class="col-xs-12">
                <div class="meta__container--without-image">
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="meta__info">
                                <span class="meta__date"><span
                                        class="glyphicon glyphicon-calendar"></span> {{ date('d.m.Y', strtotime($post['created_at'])) }} </span>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="comment-icon-counter-detail">
                                <span class="glyphicon glyphicon-comment comment-icon"></span>
                                <span class="comment-counter">{{ $post->comments->count() }}</span>
                                    <!--class="comment-counter">\models\Comments::instance()->getCountByArticleId($post['id'])</span>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-10  col-xs-offset-1  col-md-8  col-md-offset-2  push-down-60">
            <div class="post-content">
                <h1>{{ $post['caption'] }}</h1>
                <p> {!! $post['detail'] !!} </p>
            </div>
            <br>
            <div class="row">
                <div class="col-xs-12  col-sm-6">
                    <div class="social-icons">
                        <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
                        <script src="//yastatic.net/share2/share.js"></script>
                        <div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,moimir,gplus,twitter,lj,telegram"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12">
                    <div class="comments">
                        @if ($post['show_comments'] || (Auth::check() && Auth::user()->status == 1))
                            <h6>Комментарии</h6>
                            <hr>
                            <!-- Блок с комментариями -->
                            <div class="comment-list">
                                @foreach ($comments as $comment)
                                    <div data-id="{{ $comment['id'] }}" class="comment_block comment clearfix">
                                        <div class="comment-avatar pull-left">
                                            <img src="{{ $comment->user->avatar }}" alt="User Avatar" class="img-circle comment-avatar-image">
                                        </div>
                                        <div class="comment-body clearfix">
                                            <div class="comment-header">
                                                <strong class="primary-font">{{ $comment->user->name }}</strong>
                                                <small class="pull-right text-muted">
                                                    @if(Auth::check() && (Auth::user()->status === 1 || Auth::user()->id == $comment->user_id))
                                                        <span class="del_btn"
                                                              href="#"
                                                              style="display: inline-block"><img class="comment_delete"
                                                                                                 src="/assets/images/trash.png"
                                                                                                 alt="Delete"
                                                                                                 title="Удалить комментарий">
                                                    </span>
                                                    @endif
                                                    <span class="glyphicon glyphicon-time"></span> {{ date('d.m.Y H:i:s', strtotime($comment['created_at'])) }}
                                                </small>
                                            </div>
                                            <p class="comment-text">{{ $comment['text'] }}</p>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @endif
                        @if(((Auth::check() && Auth::user()->status != 3) && $post['commentable']) || (Auth::check() && Auth::user()->status == 1))
                            <div class="comment clearfix">
                                <div class="comment-avatar pull-left">
                                    <img src="{{ Auth::user()->avatar }}" alt="User Avatar" class="img-circle comment-avatar-image">
                                </div>
                                <form id="add_comment" method="get">
                                    <input type="text" class="comment_text" name="comment">
                                    <input type="hidden" name="post_id" value="{{ $post['id'] }}">
                                    <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                                    <input type="hidden" name="user_name" value="{{ Auth::user()->name }}">
                                    <input class="btn btn-primary floated-right submit" type="button" value="Комментировать">
                                </form>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection