@extends('base')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-11 col-sm-9 col-md-5 p-2 mt-1 form-border card-shadow m-auto">
            <div class="post-content">
                <h1>Регистрация</h1>
                <form enctype="multipart/form-data" method="post">
                    {{ csrf_field() }}
                    <input class="form-control" type="text" name="login" placeholder="{{ trans('site.form.name') }}" value="{{ old('login') }}">
                        @if ($errors->has('login'))
                        <span class="red-color">{{ $errors->get('login')[0] }} </span>
                        @endif <br>
                    <input class="form-control" type="password" name="password" placeholder="{{ trans('site.form.password') }}">
                        @if ($errors->has('password'))
                        <span class="red-color">{{ $errors->get('password')[0] }} </span>
                        @endif<br>
                    <input class="form-control" type="password" name="password2" placeholder="{{ trans('site.form.password_repeat') }}">
                        @if ($errors->has('password2'))
                            <span class="red-color">{{ $errors->get('password2')[0] }} </span>
                        @endif<br>
                    <input class="form-control" type="email" name="email" placeholder="{{ trans('site.form.email') }}" value="{{ old('email') }}">
                        @if ($errors->has('email'))
                        <span class="red-color">{{ $errors->get('email')[0] }} </span>
                        @endif<br>
                    <span>Аватар</span>
                    @if ($errors->has('avatar'))
                        <span class="red-color">{{ $errors->get('avatar')[0] }} </span>
                    @endif<br>
                    <input class="form-control-file" type="file" name="avatar"><br>
                    <input class="button" type="submit" name="reg" value="{{ trans('site.button.register') }}"><br>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection