@extends('base')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xl-12 mb-5">
                @include('blocks.search_form_full')
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xl-12 mb-3">
                @if(isset($cargo))
                    @php
                    $cargos = $cargo;
                    @endphp
                    @if(count($cargos) == 0)
                        <h3>Грузы не найдены</h3>
                        @else
                        <h3>Найденые грузы</h3>
                    @endif
                    @else
                        <h3>Все активные грузы</h3>
                @endif
                @foreach($cargos as $cargo)
                    <div class="card mb-2 p-2 card-shadow">
                        <div class="card-block">
                            <div class="row">
                                <div class="col-sm-12 col-xs-12 col-md-6">
                                    <h5>{{ $cargo['name'] }}</h5>
                                </div>
                                <div class="col-sm-12 col-xs-12 col-md-6">
                                    <p class="card-title font-weight-bold">c {{ date('d.m.Y', strtotime($cargo['date_start'])) }} до {{ date('d.m.Y', strtotime($cargo['date_end'])) }}</p>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-12 col-xs-12 col-md-6 mb-2">
                                    <p class="font-weight-bold">Место загрузки:</p>
                                    <p class="card-text">{{ $cargo['departure_city'] }}</p>
                                </div>
                                <div class="col-sm-12 col-xs-12 col-md-6 mb-2">
                                    <p class="font-weight-bold">Место выгрузки:</p>
                                    <p class="card-text">{{ $cargo['destination_city'] }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-xs-12 col-md-6 mb-2">
                                    <p class="font-weight-bold">Вес до:</p>
                                    <p class="card-text">{{ $cargo['weight'] }}</p>
                                </div>
                                <div class="col-sm-12 col-xs-12 col-md-6 mb-2">
                                    <p class="font-weight-bold">Объём до:</p>
                                    <p class="card-text">{{ $cargo['volume'] }}</p>
                                </div>
                            </div>
                            @if (Auth::check())
                                <a href="{{ route('site.cargo.show', $cargo['id']) }}" class="button">Подробнее</a>
                            @else
                                <p>Авторизуйтесь, чтобы увидеть подробности</p>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection