@extends('base')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-9 col-md-5 p-2 mt-1 form-border card-shadow m-auto">
            <div class="post-content">
                <h1>Авторизация</h1>
                @if(Session::has('message'))
                    {{Session::get('message')}}
                @endif
                <form method="post">
                    {{ csrf_field() }}
                    <input class="form-control" type="text" name="name" placeholder="{{ trans('site.form.name') }}" value="{{ old('name') }}"><br>
                    <input class="form-control" type="password" name="password" placeholder="{{ trans('site.form.password') }}"><br>
                    <input type="checkbox" id="remember" name="remember"><label for="remember"><span>{{ trans('site.form.remember') }}</span></label><br>
                    <input class="button mt-2" type="submit" name="auth" value="{{ trans('site.button.login') }}"><br>
                    <a class="mt-2 d-block" href="{{ route('site.auth.register') }}">{{ trans('site.button.register') }}</a>
                </form>
                @if (session('authError'))
                    <p class="red-color">{{ session('authError') }}</p>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection