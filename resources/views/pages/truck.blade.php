@extends('base')

@section('content')
    <div class="container mt-2">
        <div class="row">
            <div class="col-xl-12 mb-3">
                <div class="card mb-2 p-2 card-shadow">
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-12 col-xs-12 col-md-12">
                                <h1></h1>
                            </div>
                        </div>
                        <div class="container">
                            <div class="row vertical-align">
                                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-6 form-border pb-2 mb-2">
                                    <h3>Отправление</h3>
                                    <h4 class="red-color">{{ date('d.m.Y', strtotime($truck['date_start'])) }}</h4>
                                    <h4>{{ $truck['departure_city'] }}</h4>
                                    <p class="location-area">{{ $truck['departure_area'] }}</p>
                                </div>
                                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 d-none d-md-block">
                                    <img class="arrow-left" src="/assets/images/arrow-right.png">
                                </div>
                                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-6 form-border pb-2 mb-2">
                                    <h3>Прибытие</h3>
                                    <h4 class="red-color">{{ date('d.m.Y', strtotime($truck['date_end']))}}</h4>
                                    <h4>{{ $truck['destination_city'] }}</h4>
                                    <p class="location-area">{{ $truck['destination_area'] }}</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3 col-sm-3 col-md-4 col-sm-4 col-xs-4 mb-2">
                                <p class="font-weight-bold">Дата добавления:</p>
                            </div>
                            <div class="col-xs-9 col-sm-9 col-md-8 col-sm-8 col-xs-8 mb-2">
                                <p class="card-text">{{ date('d.m.Y', strtotime($truck['date_end'])) }}</p>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-xs-3 col-sm-3 col-md-4 col-sm-4 col-xs-4 mb-2">
                                <p class="font-weight-bold">Тип кузова:</p>
                            </div>
                            <div class="col-xs-9 col-sm-9 col-md-8 col-sm-8 col-xs-8 mb-2">
                                <ul>
                                    @if($truck['capony'] === 1)
                                        <li>Тент</li>
                                    @endif
                                    @if($truck['isotherm'] === 1)
                                        <li>Изотерма</li>
                                    @endif
                                    @if($truck['metal'] === 1)
                                        <li>Цельнометалический</li>
                                    @endif
                                    @if($truck['flatbed'] === 1)
                                        <li>Бортовой</li>
                                    @endif
                                    @if($truck['tipper'] === 1)
                                        <li>Самосвал</li>
                                    @endif
                                    @if($truck['container'] === 1)
                                        <li>Контейнер</li>
                                    @endif
                                    @if($truck['trawl'] === 1)
                                        <li>Трал</li>
                                    @endif
                                    @if($truck['tank'] === 1)
                                        <li>Цистерна</li>
                                    @endif
                                    @if($truck['grain'] === 1)
                                        <li>Зерновоз</li>
                                    @endif
                                    @if($truck['livestock'] === 1)
                                        <li>Скотовоз</li>
                                    @endif
                                    @if($truck['car'] === 1)
                                        <li>Автовоз</li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-xs-3 col-sm-3 col-md-4 col-sm-4 col-xs-4 mb-2">
                                <p class="font-weight-bold">Способ погрузки:</p>
                            </div>
                            <div class="col-xs-9 col-sm-9 col-md-8 col-sm-8 col-xs-8 mb-2">
                                <ul>
                                    @if($truck['rear'] === 1)
                                        <li>Задняя</li>
                                    @endif
                                    @if($truck['top'] === 1)
                                        <li>Верхняя</li>
                                    @endif
                                    @if($truck['side'] === 1)
                                        <li>Боковая</li>
                                    @endif
                                    @if($truck['rear-side'] === 1)
                                        <li>Задняя и боковая</li>
                                    @endif
                                    @if($truck['full'] === 1)
                                        <li>Полная растентовка</li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-xs-3 col-sm-3 col-md-4 col-sm-4 col-xs-4 mb-2">
                                <p class="font-weight-bold">Вес до:</p>
                            </div>
                            <div class="col-xs-9 col-sm-9 col-md-8 col-sm-8 col-xs-8 mb-2">
                                @if(!empty($cargo['weight']))
                                    <span>{{ $cargo['weight'] }}</span>
                                @else
                                    <span>Не указано</span>
                                @endif
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-xs-3 col-sm-3 col-md-4 col-sm-4 col-xs-4 mb-2">
                                <p class="font-weight-bold">Объём до:</p>
                            </div>
                            <div class="col-xs-9 col-sm-9 col-md-8 col-sm-8 col-xs-8 mb-2">
                                @if(!empty($cargo['volume']))
                                    <span>{{ $cargo['volume'] }}</span>
                                @else
                                    <span>Не указано</span>
                                @endif
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-xs-3 col-sm-3 col-md-4 col-sm-4 col-xs-4 mb-2">
                                <p class="font-weight-bold">Длина до:</p>
                            </div>
                            <div class="col-xs-9 col-sm-9 col-md-8 col-sm-8 col-xs-8 mb-2">
                                @if(!empty($cargo['length']))
                                    <span>{{ $cargo['length'] }}</span>
                                @else
                                    <span>Не указано</span>
                                @endif
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-xs-3 col-sm-3 col-md-4 col-sm-4 col-xs-4 mb-2">
                                <p class="font-weight-bold">Ширина до:</p>
                            </div>
                            <div class="col-xs-9 col-sm-9 col-md-8 col-sm-8 col-xs-8 mb-2">
                                @if(!empty($cargo['width']))
                                    <span>{{ $cargo['width'] }}</span>
                                @else
                                    <span>Не указано</span>
                                @endif
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-xs-3 col-sm-3 col-md-4 col-sm-4 col-xs-4 mb-2">
                                <p class="font-weight-bold">Высота до:</p>
                            </div>
                            <div class="col-xs-9 col-sm-9 col-md-8 col-sm-8 col-xs-8 mb-2">
                                @if(!empty($cargo['height']))
                                    <span>{{ $cargo['height'] }}</span>
                                @else
                                    <span>Не указано</span>
                                @endif
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-xs-3 col-sm-3 col-md-4 col-sm-4 col-xs-4 mb-2">
                                <p class="font-weight-bold">Оплата:</p>
                            </div>
                            <div class="col-xs-9 col-sm-9 col-md-8 col-sm-8 col-xs-8 mb-2">
                                <ul>
                                    @if($truck['prepay'] === 1)
                                        <li>С предоплатой</li>
                                    @endif
                                    @if($truck['nostake'] === 1)
                                        <li>Без ставки</li>
                                    @endif
                                    @if($truck['cash'] === 1)
                                        <li>Наличная оплата</li>
                                    @endif
                                    @if($truck['nonds'] === 1)
                                        <li>Безналичная без НДС</li>
                                    @endif
                                    @if($truck['nds'] === 1)
                                        <li>Безналичная с НДС</li>
                                    @endif
                                    @if($truck['card'] === 1)
                                        <li>На карту</li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-xs-3 col-sm-3 col-md-4 col-sm-4 col-xs-4 mb-2">
                                <p class="font-weight-bold">Догруз:</p>
                            </div>
                            <div class="col-xs-9 col-sm-9 col-md-8 col-sm-8 col-xs-8 mb-2">
                                <span>{{ $truck->aload->name }}</span>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-xs-3 col-sm-3 col-md-4 col-sm-4 col-xs-4 mb-2">
                                <p class="font-weight-bold">Дополнительно:</p>
                            </div>
                            <div class="col-xs-9 col-sm-9 col-md-8 col-sm-8 col-xs-8 mb-2">
                                <span>{{ $truck->extraparam->name }}</span>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-xs-3 col-sm-3 col-md-4 col-sm-4 col-xs-4 mb-2">
                                <p class="font-weight-bold">Компания:</p>
                            </div>
                            <div class="col-xs-9 col-sm-9 col-md-8 col-sm-8 col-xs-8 mb-2">
                                <span>{{ $truck['company'] }}</span>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-xs-3 col-sm-3 col-md-4 col-sm-4 col-xs-4 mb-2">
                                <p class="font-weight-bold">Телефон:</p>
                            </div>
                            <div class="col-xs-9 col-sm-9 col-md-8 col-sm-8 col-xs-8 mb-2">
                                <span>{{ $truck['telephone'] }}</span>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-xs-3 col-sm-3 col-md-4 col-sm-4 col-xs-4 mb-2">
                                <p class="font-weight-bold">Контактное лицо:</p>
                            </div>
                            <div class="col-xs-9 col-sm-9 col-md-8 col-sm-8 col-xs-8 mb-2">
                                <span>{{ $truck['contact_person'] }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection