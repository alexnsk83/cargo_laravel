@extends('base')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xl-12 mb-5">
                @include('blocks.search_form_full')
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xl-12 mb-3">
                @if(isset($truck))
                    @php
                        $trucks = $truck;
                    @endphp
                    @if(count($trucks) == 0)
                        <h3>Транспорт не найден</h3>
                    @else
                        <h3>Найденый транспорт</h3>
                    @endif
                @else
                    <h3>Весь доступный транспорт</h3>
                @endif
                @foreach($trucks as $truck)
                    <div class="card mb-2 p-2 card-shadow">
                        <div class="card-block">
                            <div class="row">
                                <div class="col-sm-12 col-xs-12 col-md-6">
                                    <h5>{{ $truck['name'] }}</h5>
                                </div>
                                <div class="col-sm-12 col-xs-12 col-md-6">
                                    <p class="card-title font-weight-bold">c {{ date('d.m.Y', strtotime($truck['date_start'])) }} до {{ date('d.m.Y', strtotime($truck['date_end'])) }}</p>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-12 col-xs-12 col-md-6 mb-2">
                                    <p class="font-weight-bold">Место загрузки:</p>
                                    <p class="card-text">{{ $truck['departure_city'] }}</p>
                                </div>
                                <div class="col-sm-12 col-xs-12 col-md-6 mb-2">
                                    <p class="font-weight-bold">Место выгрузки:</p>
                                    <p class="card-text">{{ $truck['destination_city'] }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-xs-12 col-md-6 mb-2">
                                    <p class="font-weight-bold">Вес до:</p>
                                    <p class="card-text">{{ $truck['weight'] }}</p>
                                </div>
                                <div class="col-sm-12 col-xs-12 col-md-6 mb-2">
                                    <p class="font-weight-bold">Объём до:</p>
                                    <p class="card-text">{{ $truck['volume'] }}</p>
                                </div>
                            </div>
                            @if (Auth::check())
                                <a href="{{ route('site.truck.show', $truck['id']) }}" class="button">Подробнее</a>
                            @else
                                <p>Авторизуйтесь, чтобы увидеть подробности</p>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection