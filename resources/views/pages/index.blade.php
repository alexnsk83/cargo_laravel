@extends('base')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                @include('blocks.search_form_short')
            </div>
        </div>
    </div>
    <hr>
    <div class="container mb-3">
        <div class="row">
            <div class="col-xl-12">
                <h3>Последние добавленные грузы</h3>
                @foreach($cargos as $cargo)
                <div class="card mb-2 p-2 card-shadow">
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-12 col-xs-12 col-md-6 mb-2">
                                <h5>{{ $cargo['name'] }}</h5>
                            </div>
                            <div class="col-sm-12 col-xs-12 col-md-6 mb-2">
                                <p class="card-title font-weight-bold">c {{ date('d.m.Y', strtotime($cargo['date_start'])) }} до {{ date('d.m.Y', strtotime($cargo['date_end'])) }}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-xs-12 col-md-6 mb-2">
                                <p class="card-text">Место загрузки:</p>
                                <p class="font-weight-bold">{{ $cargo['departure_city'] }}</p>
                            </div>
                            <div class="col-sm-12 col-xs-12 col-md-6 mb-2">
                                <p class="card-text">Место выгрузки:</p>
                                <p class="font-weight-bold">{{ $cargo['destination_city'] }}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-xs-12 col-md-6 mb-2">
                                <p class="card-text">Вес до:</p>
                                <p class="font-weight-bold">{{ $cargo['weight'] }}</p>
                            </div>
                            <div class="col-sm-12 col-xs-12 col-md-6 mb-2">
                                <p class="card-text">Объём до:</p>
                                <p class="font-weight-bold">{{ $cargo['volume'] }}</p>
                            </div>
                        </div>
                        @if (Auth::check())
                            <a href="{{ route('site.cargo.show', $cargo['id']) }}" class="button">Подробнее</a>
                        @else
                            <p>Авторизуйтесь, чтобы увидеть подробности</p>
                        @endif

                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="container mb-3">
        <div class="row">
            <div class="col-xl-12">
                <h3>Последний добавленный транспорт</h3>
                @foreach($trucks as $truck)
                    <div class="card mb-2 p-2 card-shadow">
                        <div class="card-block">
                                <div class="col-sm-12 col-xs-12 col-md-6 mb-2">
                                    <p class="card-title font-weight-bold">c {{ date('d.m.Y', strtotime($truck['date_start'])) }} до {{ date('d.m.Y', strtotime($truck['date_end'])) }}</p>
                                </div>
                            <div class="row">
                                <div class="col-sm-12 col-xs-12 col-md-6 mb-2">
                                    <p class="card-text">Место загрузки:</p>
                                    <p class="font-weight-bold">{{ $truck['departure_city'] }}</p>
                                </div>
                                <div class="col-sm-12 col-xs-12 col-md-6 mb-2">
                                    <p class="card-text">Место выгрузки:</p>
                                    <p class="font-weight-bold">{{ $truck['destination_city'] }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-xs-12 col-md-6 mb-2">
                                    <p class="card-text">Вес до:</p>
                                    <p class="font-weight-bold">{{ $truck['weight'] }}</p>
                                </div>
                                <div class="col-sm-12 col-xs-12 col-md-6 mb-2">
                                    <p class="card-text">Объём до:</p>
                                    <p class="font-weight-bold">{{ $truck['volume'] }}</p>
                                </div>
                            </div>
                            @if (Auth::check())
                                <a href="{{ route('site.truck.show', $truck['id']) }}" class="button">Подробнее</a>
                            @else
                                <p>Авторизуйтесь, чтобы увидеть подробности</p>
                            @endif

                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection