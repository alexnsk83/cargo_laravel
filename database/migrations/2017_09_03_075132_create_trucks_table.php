<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrucksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trucks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('departure_area');
            $table->string('departure_area_id');
            $table->string('departure_city');
            $table->string('departure_city_id');
            $table->string('destination_area');
            $table->string('destination_area_id');
            $table->string('destination_city');
            $table->string('destination_city_id');

            $table->enum('body_type', ['all', 'capony', 'frige', 'isotherm', 'metal', 'flatbed', 'tipper', 'container', 'trawl', 'tank', 'grain', 'livestock', 'car']);

            $table->tinyInteger('capony')->default(0);
            $table->tinyInteger('frige')->default(0);
            $table->tinyInteger('isotherm')->default(0);
            $table->tinyInteger('metal')->default(0);
            $table->tinyInteger('flatbed')->default(0);
            $table->tinyInteger('tipper')->default(0);
            $table->tinyInteger('container')->default(0);
            $table->tinyInteger('trawl')->default(0);
            $table->tinyInteger('tank')->default(0);
            $table->tinyInteger('grain')->default(0);
            $table->tinyInteger('livestock')->default(0);
            $table->tinyInteger('car')->default(0);

            $table->enum('load_type', ['all', 'rear', 'top', 'side', 'rear-side', 'full']);

            $table->tinyInteger('rear')->default(0)->nullable(0);
            $table->tinyInteger('top')->default(0)->nullable(0);
            $table->tinyInteger('side')->default(0)->nullable(0);
            $table->tinyInteger('rear-side')->default(0)->nullable(0);
            $table->tinyInteger('full')->default(0)->nullable(0);

            $table->timestamp('date_start');
            $table->timestamp('date_end');

            $table->tinyInteger('prepay')->default(0);
            $table->tinyInteger('nostake')->default(0);
            $table->tinyInteger('cash')->default(0);
            $table->tinyInteger('nonds')->default(0);
            $table->tinyInteger('nds')->default(0);
            $table->tinyInteger('card')->default(0);

            $table->tinyInteger('afterload')->default(1);
            $table->tinyInteger('extra')->default(1);

            $table->string('contact_person');
            $table->string('telephone');
            $table->string('company');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trucks');
    }
}
