<?php

use Illuminate\Database\Seeder;
use App\Truck;

class TrucksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('ru_RU');

        Truck::create([
            'departure_area' => 'Томская обл.',
            'departure_area_id' => '889b1f3a-98aa-40fc-9d3d-0f41192758ab',
            'departure_city' => 'Томск',
            'departure_city_id' => 'e3b0eae8-a4ce-4779-ae04-5c0797de66be',
            'destination_area' => 'Омская обл.',
            'destination_area_id' => '05426864-466d-41a3-82c4-11e61cdc98ce',
            'destination_city' => 'Омск',
            'destination_city_id' => '140e31da-27bf-4519-9ea0-6185d681d44e',
            'flatbed' => 1,
            'top' => 1,
            'rear' => 1,
            'side' => 1,
            'afterload' => 3,
            'date_start' => '2017-09-13 14:53:07',
            'date_end' => '2017-09-25 14:53:07',
            'contact_person' => $faker->firstName,
            'telephone' => $faker->phoneNumber,
            'company' => $faker->company,
        ]);

        Truck::create([
            'departure_area' => 'Томская обл.',
            'departure_area_id' => '889b1f3a-98aa-40fc-9d3d-0f41192758ab',
            'departure_city' => 'Томск',
            'departure_city_id' => 'e3b0eae8-a4ce-4779-ae04-5c0797de66be',
            'destination_area' => 'Новосибирская обл.',
            'destination_area_id' => '1ac46b49-3209-4814-b7bf-a509ea1aecd9',
            'destination_city' => 'Новосибирск',
            'destination_city_id' => '8dea00e3-9aab-4d8e-887c-ef2aaa546456',
            'capony' => 1,
            'rear' => 1,
            'afterload' => 2,
            'date_start' => '2017-09-20 14:53:07',
            'date_end' => '2017-10-01 14:53:07',
            'contact_person' => $faker->firstName,
            'telephone' => $faker->phoneNumber,
            'company' => $faker->company,
        ]);

        Truck::create([
            'departure_area' => 'Кемеровская обл.',
            'departure_area_id' => '393aeccb-89ef-4a7e-ae42-08d5cebc2e30',
            'departure_city' => 'Кемерово',
            'departure_city_id' => '94bb19a3-c1fa-410b-8651-ac1bf7c050cd',
            'destination_area' => 'Кемеровская обл.',
            'destination_area_id' => '393aeccb-89ef-4a7e-ae42-08d5cebc2e30',
            'destination_city' => 'Новокузнецк',
            'destination_city_id' => 'b28b6f6f-1435-444e-95a6-68c499b0d27a',
            'trawl' => 1,
            'full' => 1,
            'top' => 1,
            'extra' => 2,
            'date_start' => '2017-09-20 14:53:07',
            'date_end' => '2017-10-01 14:53:07',
            'contact_person' => $faker->firstName,
            'telephone' => $faker->phoneNumber,
            'company' => $faker->company,
        ]);
    }
}
