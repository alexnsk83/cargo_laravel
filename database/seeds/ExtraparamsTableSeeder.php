<?php

use Illuminate\Database\Seeder;
use App\Extraparam;

class ExtraparamsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Extraparam::create([
            'name' => 'Нет'
        ]);

        Extraparam::create([
            'name' => 'ADR'
        ]);

        Extraparam::create([
            'name' => 'Коники'
        ]);
    }
}
