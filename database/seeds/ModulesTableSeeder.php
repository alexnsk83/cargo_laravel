<?php

use Illuminate\Database\Seeder;
use App\Module;

class ModulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Module::create([
            'name' => 'Текстовая страница',
            'url' => 'site.page',
        ]);

        Module::create([
            'name' => 'Грузы',
            'url' => 'site.cargo',
        ]);

        Module::create([
            'name' => 'Транспорт',
            'url' => 'site.truck',
        ]);

        Module::create([
            'name' => 'Блог',
            'url' => 'site.home',
        ]);

        Module::create([
            'name' => 'Магазин',
            'url' => 'site.shop',
        ]);

        Module::create([
            'name' => 'Галерея',
            'url' => 'site.gallery',
        ]);
    }
}
