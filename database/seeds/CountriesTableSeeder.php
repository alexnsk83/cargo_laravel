<?php

use Illuminate\Database\Seeder;
use App\Country;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Country::create([
            'title' => 'Россия'
        ]);

        Country::create([
            'title' => 'Украина'
        ]);

        Country::create([
            'title' => 'Беларусь'
        ]);

        Country::create([
            'title' => 'Казахстан'
        ]);

        Country::create([
            'title' => 'Азербайджан'
        ]);

        Country::create([
            'title' => 'Армения'
        ]);

        Country::create([
            'title' => 'Грузия'
        ]);

        Country::create([
            'title' => 'Израиль'
        ]);

        Country::create([
            'title' => 'США'
        ]);

        Country::create([
            'title' => 'Канада'
        ]);

        Country::create([
            'title' => 'Кыргызстан'
        ]);

        Country::create([
            'title' => 'Латвия'
        ]);

        Country::create([
            'title' => 'Литва'
        ]);

        Country::create([
            'title' => 'Эстония'
        ]);

        Country::create([
            'title' => 'Молдова'
        ]);

        Country::create([
            'title' => 'Таджикистан'
        ]);

        Country::create([
            'title' => 'Туркменистан'
        ]);

        Country::create([
            'title' => 'Узбекистан'
        ]);

        Country::create([
            'title' => 'Австралия'
        ]);

        Country::create([
            'title' => 'Австрия'
        ]);

        Country::create([
            'title' => 'Албания'
        ]);

        Country::create([
            'title' => 'Алжир'
        ]);

        Country::create([
            'title' => 'Американское Самоа'
        ]);

        Country::create([
            'title' => 'Ангилья'
        ]);

        Country::create([
            'title' => 'Ангола'
        ]);

        Country::create([
            'title' => 'Андорра'
        ]);

        Country::create([
            'title' => 'Антигуа и Барбуда'
        ]);

        Country::create([
            'title' => 'Аргентина'
        ]);

        Country::create([
            'title' => 'Аруба'
        ]);

        Country::create([
            'title' => 'Афганистан'
        ]);

        Country::create([
            'title' => 'Багамы'
        ]);

        Country::create([
            'title' => 'Бангладеш'
        ]);

        Country::create([
            'title' => 'Барбадос'
        ]);

        Country::create([
            'title' => 'Бахрейн'
        ]);

        Country::create([
            'title' => 'Белиз'
        ]);

        Country::create([
            'title' => 'Бельгия'
        ]);

        Country::create([
            'title' => 'Бенин'
        ]);

        Country::create([
            'title' => 'Бермуды'
        ]);

        Country::create([
            'title' => 'Болгария'
        ]);

        Country::create([
            'title' => 'Боливия'
        ]);

        Country::create([
            'title' => 'Босния и Герцеговина'
        ]);

        Country::create([
            'title' => 'Ботсвана'
        ]);

        Country::create([
            'title' => 'Бразилия'
        ]);

        Country::create([
            'title' => 'Бруней-Даруссалам'
        ]);

        Country::create([
            'title' => 'Буркина-Фасо'
        ]);

        Country::create([
            'title' => 'Бурунди'
        ]);

        Country::create([
            'title' => 'Бутан'
        ]);

        Country::create([
            'title' => 'Вануату'
        ]);

        Country::create([
            'title' => 'Великобритания'
        ]);

        Country::create([
            'title' => 'Венгрия'
        ]);

        Country::create([
            'title' => 'Венесуэла'
        ]);

        Country::create([
            'title' => 'Виргинские острова'
        ]);

        Country::create([
            'title' => 'Виргинские острова'
        ]);

        Country::create([
            'title' => 'Восточный Тимор'
        ]);

        Country::create([
            'title' => 'Вьетнам'
        ]);

        Country::create([
            'title' => 'Габон'
        ]);

        Country::create([
            'title' => 'Гаити'
        ]);

        Country::create([
            'title' => 'Гайана'
        ]);

        Country::create([
            'title' => 'Гамбия'
        ]);

        Country::create([
            'title' => 'Гана'
        ]);

        Country::create([
            'title' => 'Гваделупа'
        ]);

        Country::create([
            'title' => 'Гватемала'
        ]);

        Country::create([
            'title' => 'Гвинея'
        ]);

        Country::create([
            'title' => 'Гвинея-Бисау'
        ]);

        Country::create([
            'title' => 'Германия'
        ]);

        Country::create([
            'title' => 'Гибралтар'
        ]);

        Country::create([
            'title' => 'Гондурас'
        ]);

        Country::create([
            'title' => 'Гонконг'
        ]);

        Country::create([
            'title' => 'Гренада'
        ]);

        Country::create([
            'title' => 'Гренландия'
        ]);

        Country::create([
            'title' => 'Греция'
        ]);

        Country::create([
            'title' => 'Гуам'
        ]);

        Country::create([
            'title' => 'Дания'
        ]);

        Country::create([
            'title' => 'Доминика'
        ]);

        Country::create([
            'title' => 'Доминиканская Республика'
        ]);

        Country::create([
            'title' => 'Египет'
        ]);

        Country::create([
            'title' => 'Замбия'
        ]);

        Country::create([
            'title' => 'Западная Сахара'
        ]);

        Country::create([
            'title' => 'Зимбабве'
        ]);

        Country::create([
            'title' => 'Индия'
        ]);

        Country::create([
            'title' => 'Индонезия'
        ]);

        Country::create([
            'title' => 'Иордания'
        ]);

        Country::create([
            'title' => 'Ирак'
        ]);

        Country::create([
            'title' => 'Иран'
        ]);

        Country::create([
            'title' => 'Ирландия'
        ]);

        Country::create([
            'title' => 'Исландия'
        ]);

        Country::create([
            'title' => 'Испания'
        ]);

        Country::create([
            'title' => 'Италия'
        ]);

        Country::create([
            'title' => 'Йемен'
        ]);

        Country::create([
            'title' => 'Кабо-Верде'
        ]);

        Country::create([
            'title' => 'Камбоджа'
        ]);

        Country::create([
            'title' => 'Камерун'
        ]);

        Country::create([
            'title' => 'Катар'
        ]);

        Country::create([
            'title' => 'Кения'
        ]);

        Country::create([
            'title' => 'Кипр'
        ]);

        Country::create([
            'title' => 'Кирибати'
        ]);

        Country::create([
            'title' => 'Китай'
        ]);

        Country::create([
            'title' => 'Колумбия'
        ]);

        Country::create([
            'title' => 'Коморы'
        ]);

        Country::create([
            'title' => 'Конго'
        ]);

        Country::create([
            'title' => 'Конго'
        ]);

        Country::create([
            'title' => 'Коста-Рика'
        ]);

        Country::create([
            'title' => 'Кот д`Ивуар'
        ]);

        Country::create([
            'title' => 'Куба'
        ]);

        Country::create([
            'title' => 'Кувейт'
        ]);

        Country::create([
            'title' => 'Лаос'
        ]);

        Country::create([
            'title' => 'Лесото'
        ]);

        Country::create([
            'title' => 'Либерия'
        ]);

        Country::create([
            'title' => 'Ливан'
        ]);

        Country::create([
            'title' => 'Ливия'
        ]);

        Country::create([
            'title' => 'Лихтенштейн'
        ]);

        Country::create([
            'title' => 'Люксембург'
        ]);

        Country::create([
            'title' => 'Маврикий'
        ]);

        Country::create([
            'title' => 'Мавритания'
        ]);

        Country::create([
            'title' => 'Мадагаскар'
        ]);

        Country::create([
            'title' => 'Макао'
        ]);

        Country::create([
            'title' => 'Македония'
        ]);

        Country::create([
            'title' => 'Малави'
        ]);

        Country::create([
            'title' => 'Малайзия'
        ]);

        Country::create([
            'title' => 'Мали'
        ]);

        Country::create([
            'title' => 'Мальдивы'
        ]);

        Country::create([
            'title' => 'Мальта'
        ]);

        Country::create([
            'title' => 'Марокко'
        ]);

        Country::create([
            'title' => 'Мартиника'
        ]);

        Country::create([
            'title' => 'Маршалловы Острова'
        ]);

        Country::create([
            'title' => 'Мексика'
        ]);

        Country::create([
            'title' => 'Микронезия'
        ]);

        Country::create([
            'title' => 'Мозамбик'
        ]);

        Country::create([
            'title' => 'Монако'
        ]);

        Country::create([
            'title' => 'Монголия'
        ]);

        Country::create([
            'title' => 'Монтсеррат'
        ]);

        Country::create([
            'title' => 'Мьянма'
        ]);

        Country::create([
            'title' => 'Намибия'
        ]);

        Country::create([
            'title' => 'Науру'
        ]);

        Country::create([
            'title' => 'Непал'
        ]);

        Country::create([
            'title' => 'Нигер'
        ]);

        Country::create([
            'title' => 'Нигерия'
        ]);

        Country::create([
            'title' => 'Кюрасао'
        ]);

        Country::create([
            'title' => 'Нидерланды'
        ]);

        Country::create([
            'title' => 'Никарагуа'
        ]);

        Country::create([
            'title' => 'Ниуэ'
        ]);

        Country::create([
            'title' => 'Новая Зеландия'
        ]);

        Country::create([
            'title' => 'Новая Каледония'
        ]);

        Country::create([
            'title' => 'Норвегия'
        ]);

        Country::create([
            'title' => 'Объединенные Арабские Эмираты'
        ]);

        Country::create([
            'title' => 'Оман'
        ]);

        Country::create([
            'title' => 'Остров Мэн'
        ]);

        Country::create([
            'title' => 'Остров Норфолк'
        ]);

        Country::create([
            'title' => 'Острова Кайман'
        ]);

        Country::create([
            'title' => 'Острова Кука'
        ]);

        Country::create([
            'title' => 'Острова Теркс и Кайкос'
        ]);

        Country::create([
            'title' => 'Пакистан'
        ]);

        Country::create([
            'title' => 'Палау'
        ]);

        Country::create([
            'title' => 'Палестинская автономия'
        ]);

        Country::create([
            'title' => 'Панама'
        ]);

        Country::create([
            'title' => 'Папуа - Новая Гвинея'
        ]);

        Country::create([
            'title' => 'Парагвай'
        ]);

        Country::create([
            'title' => 'Перу'
        ]);

        Country::create([
            'title' => 'Питкерн'
        ]);

        Country::create([
            'title' => 'Польша'
        ]);

        Country::create([
            'title' => 'Португалия'
        ]);

        Country::create([
            'title' => 'Пуэрто-Рико'
        ]);

        Country::create([
            'title' => 'Реюньон'
        ]);

        Country::create([
            'title' => 'Руанда'
        ]);

        Country::create([
            'title' => 'Румыния'
        ]);

        Country::create([
            'title' => 'Сальвадор'
        ]);

        Country::create([
            'title' => 'Самоа'
        ]);

        Country::create([
            'title' => 'Сан-Марино'
        ]);

        Country::create([
            'title' => 'Сан-Томе и Принсипи'
        ]);

        Country::create([
            'title' => 'Саудовская Аравия'
        ]);

        Country::create([
            'title' => 'Свазиленд'
        ]);

        Country::create([
            'title' => 'Святая Елена'
        ]);

        Country::create([
            'title' => 'Северная Корея'
        ]);

        Country::create([
            'title' => 'Северные Марианские острова'
        ]);

        Country::create([
            'title' => 'Сейшелы'
        ]);

        Country::create([
            'title' => 'Сенегал'
        ]);

        Country::create([
            'title' => 'Сент-Винсент'
        ]);

        Country::create([
            'title' => 'Сент-Китс и Невис'
        ]);

        Country::create([
            'title' => 'Сент-Люсия'
        ]);

        Country::create([
            'title' => 'Сент-Пьер и Микелон'
        ]);

        Country::create([
            'title' => 'Сербия'
        ]);

        Country::create([
            'title' => 'Сингапур'
        ]);

        Country::create([
            'title' => 'Сирийская Арабская Республика'
        ]);

        Country::create([
            'title' => 'Словакия'
        ]);

        Country::create([
            'title' => 'Словения'
        ]);

        Country::create([
            'title' => 'Соломоновы Острова'
        ]);

        Country::create([
            'title' => 'Сомали'
        ]);

        Country::create([
            'title' => 'Судан'
        ]);

        Country::create([
            'title' => 'Суринам'
        ]);

        Country::create([
            'title' => 'Сьерра-Леоне'
        ]);

        Country::create([
            'title' => 'Таиланд'
        ]);

        Country::create([
            'title' => 'Тайвань'
        ]);

        Country::create([
            'title' => 'Танзания'
        ]);

        Country::create([
            'title' => 'Того'
        ]);

        Country::create([
            'title' => 'Токелау'
        ]);

        Country::create([
            'title' => 'Тонга'
        ]);

        Country::create([
            'title' => 'Тринидад и Тобаго'
        ]);

        Country::create([
            'title' => 'Тувалу'
        ]);

        Country::create([
            'title' => 'Тунис'
        ]);

        Country::create([
            'title' => 'Турция'
        ]);

        Country::create([
            'title' => 'Уганда'
        ]);

        Country::create([
            'title' => 'Уоллис и Футуна'
        ]);

        Country::create([
            'title' => 'Уругвай'
        ]);

        Country::create([
            'title' => 'Фарерские острова'
        ]);

        Country::create([
            'title' => 'Фиджи'
        ]);

        Country::create([
            'title' => 'Филиппины'
        ]);

        Country::create([
            'title' => 'Финляндия'
        ]);

        Country::create([
            'title' => 'Фолклендские острова'
        ]);

        Country::create([
            'title' => 'Франция'
        ]);

        Country::create([
            'title' => 'Французская Гвиана'
        ]);

        Country::create([
            'title' => 'Французская Полинезия'
        ]);

        Country::create([
            'title' => 'Хорватия'
        ]);

        Country::create([
            'title' => 'Центрально-Африканская Республика'
        ]);

        Country::create([
            'title' => 'Чад'
        ]);

        Country::create([
            'title' => 'Чехия'
        ]);

        Country::create([
            'title' => 'Чили'
        ]);

        Country::create([
            'title' => 'Швейцария'
        ]);

        Country::create([
            'title' => 'Швеция'
        ]);

        Country::create([
            'title' => 'Шпицберген и Ян Майен'
        ]);

        Country::create([
            'title' => 'Шри-Ланка'
        ]);

        Country::create([
            'title' => 'Эквадор'
        ]);

        Country::create([
            'title' => 'Экваториальная Гвинея'
        ]);

        Country::create([
            'title' => 'Эритрея'
        ]);

        Country::create([
            'title' => 'Эфиопия'
        ]);

        Country::create([
            'title' => 'Южная Корея'
        ]);

        Country::create([
            'title' => 'Южно-Африканская Республика'
        ]);

        Country::create([
            'title' => 'Ямайка'
        ]);

        Country::create([
            'title' => 'Япония'
        ]);

        Country::create([
            'title' => 'Черногория'
        ]);

        Country::create([
            'title' => 'Джибути'
        ]);

        Country::create([
            'title' => 'Южный Судан'
        ]);

        Country::create([
            'title' => 'Ватикан'
        ]);

        Country::create([
            'title' => 'Синт-Мартен'
        ]);

        Country::create([
            'title' => 'Бонайре'
        ]);


    }
}
