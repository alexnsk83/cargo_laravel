<?php

use Illuminate\Database\Seeder;
use App\Afterload;

class AfterloadsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Afterload::create([
            'name' => 'Не важно',
        ]);

        Afterload::create([
            'name' => 'Только догруз',
        ]);

        Afterload::create([
            'name' => 'Исключить догруз',
        ]);
    }
}
