<?php
ini_set('memory_limit', '650M');
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PostsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(UserstatusesTableSeeder::class);
        $this->call(ModulesTableSeeder::class);
        $this->call(CargosTableSeeder::class);
        $this->call(TrucksTableSeeder::class);
        $this->call(AfterloadsTableSeeder::class);
        $this->call(ExtraparamsTableSeeder::class);
        $this->call(LocationsTableSeeder::class);
    }
}