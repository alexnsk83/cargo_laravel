<?php

use Illuminate\Database\Seeder;
use App\Cargo;

class CargosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('ru_RU');

        Cargo::create([
            'name' => "Теплоизолятор",
            'departure_area' => 'Томская обл.',
            'departure_area_id' => '889b1f3a-98aa-40fc-9d3d-0f41192758ab',
            'departure_city' => 'Томск',
            'departure_city_id' => 'e3b0eae8-a4ce-4779-ae04-5c0797de66be',
            'destination_area' => 'Новосибирская обл.',
            'destination_area_id' => '1ac46b49-3209-4814-b7bf-a509ea1aecd9',
            'destination_city' => 'Бердск',
            'destination_city_id' => 'fd0191f4-4687-42d9-be64-4ebc7824bd08',
            'distance' => 260,
            'capony' => 1,
            'flatbed' => 1,
            'rear' => 1,
            'weight' => '10',
            'volume' => '5',
            'prepay' => '1',
            'afterload' => 3,
            'date_start' => '2017-08-13 14:53:07',
            'date_end' => '2017-08-25 14:53:07',
            'contact_person' => $faker->firstName,
            'telephone' => $faker->phoneNumber,
            'company' => $faker->company,
        ]);

        Cargo::create([
            'name' => "Кирпич",
            'departure_area' => 'Томская обл.',
            'departure_area_id' => '889b1f3a-98aa-40fc-9d3d-0f41192758ab',
            'departure_city' => 'Томск',
            'departure_city_id' => 'e3b0eae8-a4ce-4779-ae04-5c0797de66be',
            'destination_area' => 'Новосибирская обл.',
            'destination_area_id' => '1ac46b49-3209-4814-b7bf-a509ea1aecd9',
            'destination_city' => 'Новосибирск',
            'destination_city_id' => '8dea00e3-9aab-4d8e-887c-ef2aaa546456',
            'distance' => 260,
            'capony' => 1,
            'tipper' => 1,
            'side' => 1,
            'top' => 1,
            'weight' => '20',
            'volume' => '5',
            'prepay' => '1',
            'cash' => '1',
            'afterload' => 2,
            'extra' => 3,
            'date_start' => '2017-08-20 14:53:07',
            'date_end' => '2017-09-01 14:53:07',
            'contact_person' => $faker->firstName,
            'telephone' => $faker->phoneNumber,
            'company' => $faker->company,
        ]);

        Cargo::create([
            'name' => "Брус",
            'departure_area' => 'Новосибирская обл.',
            'departure_area_id' => '1ac46b49-3209-4814-b7bf-a509ea1aecd9',
            'departure_city' => 'Новосибирск',
            'departure_city_id' => '8dea00e3-9aab-4d8e-887c-ef2aaa546456',
            'destination_area' => 'Кемеровская обл.',
            'destination_area_id' => '393aeccb-89ef-4a7e-ae42-08d5cebc2e30',
            'destination_city' => 'Новокузнецк',
            'destination_city_id' => 'b28b6f6f-1435-444e-95a6-68c499b0d27a',
            'distance' => 260,
            'trawl' => 1,
            'full' => 1,
            'top' => 1,
            'weight' => '20',
            'volume' => '10',
            'prepay' => '1',
            'nonds' => '1',
            'extra' => 3,
            'date_start' => '2017-08-20 14:53:07',
            'date_end' => '2017-09-01 14:53:07',
            'contact_person' => $faker->firstName,
            'telephone' => $faker->phoneNumber,
            'company' => $faker->company,
        ]);
    }
}
