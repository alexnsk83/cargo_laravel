<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Post;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class PostController extends Controller
{
    public function showAll()
    {
        $posts = Post::latest()->Paginate(5);

        $latestPosts = Cache::remember('latestPosts', 10, function () {
            return Post::latest()->limit(4)->get();
        });

        $popularPosts = Cache::remember('popularPosts', 10, function () {
            return Post::orderBy('views', 'DESC')->limit(4)->get();
        });

        $title = trans('site.title.main');

        return view('pages.index', compact('posts', 'popularPosts', 'latestPosts', 'title'));
    }

    public function showPost($id)
    {
        $post = Post::find($id);
        if(!$post){
            abort(404);
        }
        $post->increment('views');
        $title = $post->caption;
        $comments = Comment::where('post_id', $id)->get();

        return view('pages.post', compact('post', 'title', 'comments'));
    }

    public function addComment()
    {
        if($this->request->comment == null) {
            return redirect()->back();
        }

        $comment = new Comment();

        $comment->text = $this->request->comment;
        $comment->post_id = $this->request->post_id;
        $comment->user_id = Auth::user()->id;

        $comment->save();

        return redirect()->back();
    }
}
