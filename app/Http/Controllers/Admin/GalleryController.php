<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Gallery;
use App\Photo;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;


class GalleryController extends Controller
{
    /** Список галерей
     * @param bool $trashed
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($trashed = false)
    {
        $title = 'Галереи';
        $galleries = $trashed ? Gallery::latest()->onlyTrashed()->get() : Gallery::latest()->get();

        return view('admin.pages.gallery_list', compact('title', 'galleries', 'trashed'));
    }


    /** Список удалённых галерей
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showTrashed(){
        return $this->index(true);
    }


    /** Форма добавления новой галереи
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public  function add()
    {
        $title = trans('site.title.gallery');

        return view('admin.pages.gallery_add', compact('title'));
    }


    /** Добавление новой галереи
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addPost()
    {
        $this->validate($this->request, [
            'name' => 'required|unique:galleries|min:3',
            'slug' => 'required|unique:galleries|min:3',
            'description' => 'required|min:3',
            'cover_image' => 'file|image',
        ]);

        $cover_image = $this->request->file('cover_image');
        if($cover_image) {
            $cover_image_filename = Carbon::now()->micro . $cover_image->getClientOriginalName();
            $cover_image->move(public_path() . '/assets/images/uploads/gallery/covers/', $cover_image_filename);
            $thumb = Image::make(public_path() . '/assets/images/uploads/gallery/covers/' . $cover_image_filename);
            $thumb->fit(320, 240)
                ->save(public_path() . '/assets/images/uploads/gallery/covers/thumb_' . $cover_image_filename);
        } else {
            $cover_image_filename = 'noimage.png';
        }

        $gallery = new Gallery();
        $gallery->name = $this->request->name;
        $gallery->slug = $this->request->slug;
        $gallery->description = json_encode($this->request->description);
        $gallery->cover_image = '/assets/images/uploads/gallery/covers/' . $cover_image_filename;
        $gallery->cover_image_thumbnail = '/assets/images/uploads/gallery/covers/thumb_' . $cover_image_filename;
        $gallery->save();

        if($this->request->file('content_images')){
            $this->saveImages($this->request->file('content_images'), $gallery->id);
        }

        return redirect()->route('admin.gallery.all')->with('message', trans('messages.gallery_created'));
    }


    /** Форма редактирования галереи
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $title = trans('site.title.gallery');
        $gallery = Gallery::withTrashed()
            ->findOrFail($id);
        $photos = $gallery->photos->sortBy('order');

        return view('admin.pages.gallery_edit', compact('title', 'gallery', 'photos'));
    }


    /** Сохранение изменений в галереи
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editPost($id)
    {
        $gallery = Gallery::withTrashed()
            ->where('id', $id)->first();

        if (!$this->request->cancel) {
            $this->validate($this->request, [
                'name' => 'required|min:3',
                'slug' => 'required|min:3',
                'description' => 'required|min:3',
                'cover_image' => 'file|image',
            ]);

            if ($this->request->cover_image) {
                $cover_image = $this->request->file('cover_image');
                $cover_image_filename = Carbon::now()->micro . $cover_image->getClientOriginalName();
                $cover_image->move(public_path() . '/assets/images/uploads/gallery/covers/', $cover_image_filename);
                $thumb = Image::make(public_path() . '/assets/images/uploads/gallery/covers/' . $cover_image_filename);
                $thumb->fit(320, 240)
                    ->save(public_path() . '/assets/images/uploads/gallery/covers/thumb_' . $cover_image_filename);
                $gallery->update([
                    'cover_image' => '/assets/images/uploads/gallery/covers/' . $cover_image_filename,
                    'cover_image_thumbnail' => '/assets/images/uploads/gallery/covers/thumb_' . $cover_image_filename,
                ]);
            }

            $gallery->update([
                    'name' => $this->request->name,
                    'description' => json_encode($this->request->description),
                    'slug' => $this->request->slug,
                ]);

            //$this->changeOrder($this->request->order);
            $this->updateImagesDescription($this->request->image_description);
            $this->deleteImages($this->request->delete);

            if($this->request->file('content_images')){
                $this->saveImages($this->request->file('content_images'), $gallery->id);
            }

            if ($this->request->save) {
                return redirect()->back();
            }
        }

        $trash = $gallery->trashed();

        return $this->index($trash);
    }


    /** Удаление галереи
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        Gallery::where('id', $id)->delete();

        return redirect()->route('admin.gallery.all')->with('message', trans('messages.gallery_deleted'));
    }


    /** Восстановление галереи
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function recover($id)
    {
        Gallery::onlyTrashed()
            ->where('id', $id)
            ->restore();

        return redirect()->route('admin.gallery.trashed')->with('message', trans('messages.gallery_recovered'));
    }


    /**
     * Сохранение порядка изображений при перетаскивании
     */
    public function changeOrderWithAjax()
    {
        $list = json_decode($_POST['list']);
        foreach($list as $order => $id) {
            Photo::find($id)->update([
                'order' => $order,
            ]);
        }
    }


    /** Сохранение изображений галереи
     * @param $files
     * @param $id
     */
    private function saveImages($files, $id)
    {
        foreach ($files as $file) {
            $rules = ['file' => 'file|mimes:png,gif,jpeg'];
            $validator = Validator::make(['file' => $file], $rules);
            if ($validator->passes()) {
                $destinationPath = public_path() . "/assets/images/uploads/gallery/$id";
                $filename = Carbon::now()->micro . $file->getClientOriginalName();
                $file->move($destinationPath, $filename);

                $thumb = Image::make($destinationPath . '/' . $filename);
                $thumb->fit(150)->save($destinationPath . '/thumb_' . $filename);

                $photo = new Photo();
                $photo->gallery_id = $id;
                $photo->image = "/assets/images/uploads/gallery/$id/" . $filename;
                $photo->thumbnail = "/assets/images/uploads/gallery/$id/thumb_" . $filename;
                $photo->save();
            }
        }
    }


    /** Удаление изображения
     * @param $images
     */
    private function deleteImages($images)
    {
        if ($images) {
            foreach ($images as $id) {
                Photo::where('id', $id)->delete();
            }
        }
    }


    /** Обновление описаний изображений
     * @param $images
     */
    private function updateImagesDescription($images)
    {
        foreach ($images as $id => $description) {
            if ($description) {
                $image = Photo::where('id', $id)->first();
                if ($image->description != $description) {
                    $image->update(['description' => $description]);
                }
            }
        }
    }
}
