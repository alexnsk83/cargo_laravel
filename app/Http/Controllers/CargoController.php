<?php

namespace App\Http\Controllers;

use App\Afterload;
use App\Extraparam;
use App\Location;
use App\Truck;
use Illuminate\Http\Request;
use App\Cargo;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;

class CargoController extends Controller
{
    public function showCargos($search_result = null)
    {
        $title = trans('site.title.cargo');
        $afterloads = Afterload::all();
        $extraparams = Extraparam::all();
        $cargos = Cargo::all();
        $departure_areas = Location::where('AOLEVEL', '=', '1')->orderBy('FORMALNAME')->get();
        $destination_areas = $departure_areas;
        $cargo = $search_result[0];
        $input = $search_result[1];

        return view('pages.cargos', compact('title', 'cargos', 'afterloads', 'extraparams', 'departure_areas', 'destination_areas', 'cargo', 'input'));
    }

    public function showTrucks($search_result = null)
    {
        $title = trans('site.title.truck');
        $afterloads = Afterload::all();
        $extraparams = Extraparam::all();
        $trucks = Truck::all();
        $departure_areas = Location::where('AOLEVEL', '=', '1')->orderBy('FORMALNAME')->get();
        $destination_areas = $departure_areas;
        $truck = $search_result[0];
        $input = $search_result[1];

        return view('pages.trucks', compact('title', 'trucks', 'afterloads', 'extraparams', 'departure_areas', 'destination_areas', 'truck', 'input'));
    }

    /** Детали выбранного груза
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showCargo($id)
    {
        $cargo = Cargo::find($id);
        if(!$cargo){
            abort(404);
        }
        $title = $cargo->name;

        return view('pages.cargo', compact('title', 'cargo'));
    }

    /** Детали выбранного транспорта
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showTruck($id)
    {
        $truck = Truck::find($id);
        if(!$truck){
            abort(404);
        }
        $title = trans('site.title.truck');

        return view('pages.truck', compact('title', 'truck'));
    }

    /** Определяем, что искать, грузы или транспорт
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function find()
    {
        $this->validate($this->request, [

        ]);
        $input = Input::all();

        if(isset($input['cargo'])) {
            $result = $this->findCargo($input);

            return $this->showCargos($result);
        } else {
            $result = $this->findTruck($input);

            return $this->showTrucks($result);
        }
    }
    
    protected function findCargo($input)
    {
        $cargo = Cargo::all();
        $cargo = $this->search($input, $cargo);

        return [$cargo, $input];
    }

    protected function findTruck($input)
    {
        $truck = Truck::all();
        $truck = $this->search($input, $truck);

        return [$truck, $input];
    }

    /** Поиск грузов или транспорта по заданным параметрам
     * @param $input
     * @param $collection
     * @return mixed
     */
    protected function search($input, $collection)
    {
        if(isset($input['departure_area'])) {
            $collection = $collection->where('departure_area_id', '=', $input['departure_area']);
        }
        if(isset($input['departure_city'])) {
            $collection = $collection->where('departure_city_id', '=', $input['departure_city']);
        }
        if(isset($input['destination_area'])) {
            $collection = $collection->where('destination_area_id', '=', $input['destination_area']);
        }
        if(isset($input['destination_city'])) {
            $collection = $collection->where('destination_city_id', '=', $input['destination_city']);
        }
        if(isset($input['weight_min'])) {
            $collection = $collection->where('weight', '>=', $input['weight_min']);
        }
        if(isset($input['weight_max'])) {
            $collection = $collection->where('weight', '<=', $input['weight_max']);
        }
        if(isset($input['volume_min'])) {
            $collection = $collection->where('volume', '>=', $input['volume_min']);
        }
        if(isset($input['volume_max'])) {
            $collection = $collection->where('volume', '<=', $input['volume_max']);
        }
        if(isset($input['date_start_min'])) {
            $collection = $collection->where('date_start', '>=', $input['date_start_min']);
        }
        if(isset($input['date_start_max'])) {
            $collection = $collection->where('date_start', '>=', $input['date_start_max']);
        }
        if(isset($input['date_end_min'])) {
            $collection = $collection->where('date_start', '>=', $input['date_start_min']);
        }
        if(isset($input['date_end_max'])) {
            $collection = $collection->where('date_end', '>=', $input['date_end_max']);
        }
        if(isset($input['afterloads']) && $input['afterloads'] != 1) {
            $collection = $collection->where('afterload', '=', $input['afterloads']);
        }
        if(isset($input['extra']) && $input['extra'] != 1) {
            $collection = $collection->where('extra', '=', $input['extra']);
        }

        $body = [];
        $result = []; //получаем список допустимых типов кузовов
        if (isset($input['capony'])) $body['capony'] = 1;
        if (isset($input['frige'])) $body['frige'] = 1;
        if (isset($input['isotherm'])) $body['isotherm'] = 1;
        if (isset($input['metal'])) $body['metal'] = 1;
        if (isset($input['flatbed'])) $body['flatbed'] = 1;
        if (isset($input['tipper'])) $body['tipper'] = 1;
        if (isset($input['container'])) $body['container'] = 1;
        if (isset($input['trawl'])) $body['trawl'] = 1;
        if (isset($input['tank'])) $body['tank'] = 1;
        if (isset($input['grain'])) $body['grain'] = 1;
        if (isset($input['livestock'])) $body['livestock'] = 1;
        if (isset($input['car'])) $body['car'] = 1;
        foreach ($body as $k => $v) { //Получаем выборки по выбранным типам кузовов
            $result[] = $collection->where($k , '=', $v, 'or');
        }

        if(count($result)) {
            unset($body);
            $all = collect();
            foreach ($result as $item) {
                $all = $all->merge($item);
            }
            $collection = $all;
        }

        $load = [];
        $result = [];   //получаем список допустимых способов загрузки
        if (isset($input['rear'])) $load['rear'] = 1;
        if (isset($input['top'])) $load['top'] = 1;
        if (isset($input['side'])) $load['side'] = 1;
        if (isset($input['rear-side'])) $load['rear-side'] = 1;
        if (isset($input['full'])) $load['full'] = 1;
        foreach ($load as $k => $v) {
            $result[] = $collection->where($k , '=', $v, 'or');
        }
        if(count($result)) {
            unset($load);
            $all = collect();
            foreach ($result as $item) {
                $all = $all->merge($item);
            }
            $collection = $all;
        }

        $pay = [];
        $result = [];
        if (isset($input['prepay'])) $pay['prepay'] = 1;
        if (isset($input['nostake'])) $pay['nostake'] = 1;
        if (isset($input['cash'])) $pay['cash'] = 1;
        if (isset($input['nonds'])) $pay['nonds'] = 1;
        if (isset($input['nds'])) $pay['nds'] = 1;
        if (isset($input['card'])) $pay['card'] = 1;
        foreach ($pay as $k => $v) {
            $result[] = $collection->where($k , '=', $v, 'or');
        }
        if(count($result)) {
            unset($load);
            $all = collect();
            foreach ($result as $item) {
                $all = $all->merge($item);
            }
            $collection = $all;
        }

        return $collection;
    }
}
