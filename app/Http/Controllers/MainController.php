<?php

namespace App\Http\Controllers;

use App\Cargo;
use App\Comment;
use App\Location;
use App\Post;
use App\Truck;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class MainController extends Controller
{
    public function index()
    {
        $title = trans('site.title.main');
        //$posts = Post::latest()->Paginate(5);
        $cargos = Cargo::latest()->limit(5)->get();
        $trucks = Truck::latest()->limit(5)->get();
        $departure_areas = Location::where('AOLEVEL', '=', '1')->orderBy('FORMALNAME')->get();
        $destination_areas = $departure_areas;

        return view('pages.index', compact('title', 'cargos', 'trucks', 'departure_areas', 'destination_areas'));
    }

    protected function findCity()
    {
        $all = Location::where('PARENTGUID', '=', $_POST['id'])->where("AOLEVEL", '=', '4')->orderBy('FORMALNAME')->get();
        echo json_encode($all);
    }
}
