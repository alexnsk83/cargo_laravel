<?php

namespace App\Http\Controllers\Cabinet;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class MainController extends Controller
{
    public function index()
    {
        $title = 'Личный кабинет';
        return view('cabinet.pages.index', compact('title'));
    }
}
