<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /** Подмена аватара пользователя заглушкой если файл с аватаром не найден
     * @param $avatar
     * @return mixed
     */
    protected function isAvatarExists($avatar)
    {
        if (!file_exists(public_path() . $avatar)) {
            $avatar = config('site.empty_avatar');
        }

        return $avatar;
    }
}
