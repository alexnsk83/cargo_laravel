<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 29.03.2017
 * Time: 23:29
 */

namespace App\Helpers;


class CheckAvatar
{
    /** Подмена аватара пользователя заглушкой если файл с аватаром не найден
     * @param $avatar
     * @return mixed
     */
    public static function isAvatarExists($avatar)
    {
        if (!file_exists(public_path() . $avatar)) {
            $avatar = config('site.empty_avatar');
        }

        return $avatar;
    }
}