<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Truck extends Model
{
    protected $guarded = [];

    public function aload()
    {
        return $this->belongsTo('App\Afterload', 'afterload');
    }

    public function extraparam()
    {
        return $this->belongsTo('App\Extraparam', 'extra');
    }
}
