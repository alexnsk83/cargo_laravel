$(function () {
    //События
    //Отправка комментария нажанием кнопки "Отправить"
    $('.submit').on('click', function () {
        $(this).attr('disabled', true);
        if($('.comment_text').val()){
            addCommentHandler();
        }
        $(this).attr('disabled', false);
    });

    //Отправка комментария нажанием "Enter"
    $('.comment_text').keypress(function (e) {
        if(e.keyCode == 13) {
            if($('.comment_text').val()){
                addCommentHandler();
            }
            e.preventDefault();
        }
    });

    //Удаление комментария
    $('.comment-list').on("click", '.comment_delete', function () {
        var comment = $(this).parents('.comment_block').attr('data-id');
        $.ajax({
            url: 'comment/delete',
            type: 'GET',
            data: { id: comment }
        });
        $(this).parents('.comment_block').slideUp();
    });

    //Выбор даты для инпутов с классом datepicker
    $('.datepicker').datepicker({
        monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
            'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
        dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        dateFormat: "d.m.yy"
    });

    //Автозаполнение поля города отпраления
    /*$("[name='departure']").keyup(function () {
        if ($(this).val().length > 2) {
            findCity();
        }
    });*/

    //Прилипание меню к верхнему краю экрана
    $(window).on("scroll", function() {
        var h = $('header').height()+$('.logo-block').height();
        if ($(window).scrollTop() > h) {
            $('.sticky-placeholder').height($('#sticky').height());
            $('#sticky').addClass('fixed');
        }
        else {
            $('#sticky').removeClass('fixed');
            $('.sticky-placeholder').height(0);
        }
    });



    //Функции
    function addCommentHandler() {
        $.ajax({
            url: 'comment/add',
            type: 'GET',
            data: $('#add_comment').serialize(),
            success: addComment
        });
        $('.comment').val('');
    }

    function addComment(response) {
        response = JSON.parse(response);
        if (response['del_btn']){
            response['del_btn'] = '<span class="del_btn" href="#" style="display: inline-block"><img class="comment_delete" src="/assets/images/trash.png" alt="Delete" title="Удалить комментарий"> </span>'
        }
        var content = '<div data-id="' + response['id'] + '" class="comment_block comment clearfix last_comment ">' +
            '<div class="comment-avatar pull-left">' +
            '<img src="' + response['image'] + '" alt="User Avatar" class="img-circle comment-avatar-image">' +
            '</div>' +
            '<div class="comment-body clearfix">' +
            '<div class="comment-header">' +
            '<strong class="primary-font">' + response['user_name'] + '</strong>' +
            '<small class="pull-right text-muted">' +
            response["del_btn"] +
            '<span class="glyphicon glyphicon-time"></span>' + response['created_at'] +
            '</small>' +
            '</div>' +
            '<p class="comment-text">'+ response['comment'] +'</p>' +
            '</div>' +
            '</div>';

        $('.comment-list').append(content);
        $('.last_comment').last().hide();
        $('.last_comment').last().slideDown();

        $('.comment_text').val('');
    }

    //Заполнение списка городов в поисковой форме
    $('#departure_area').on('change', function () {
        var value_selected = this.value;
        findCity(value_selected, "departure");
    });

    $('#destination_area').on('change', function () {
        var value_selected = this.value;
        findCity(value_selected, "destination");
    });

    $(window).on('load', function () {
        var dep_area_id = $('#departure_area').val();
        if(dep_area_id) {
            var dep_city_id = $('#departure_city option:selected').val();
            findCity(dep_area_id, 'departure', dep_city_id);
        }
    });

    $(window).on('load', function () {
        var des_area_id = $('#destination_area').val();
        if(des_area_id) {
            var des_city_id = $('#destination_city option:selected').val();
            findCity(des_area_id, 'destination', des_city_id);
        }
    });

    function findCity(id, direction, city) {
        $.ajax({
            url: '/city/find',
            type: 'POST',
            data: {id:id},
            success: function (res) {
                fillCityList(res, direction, id, city)
            }
        });
    }

    function fillCityList(res, direction, parent, city) {
        $('#' + direction + "_city").find('option').remove();
        if(res.length > 2) {
            res = JSON.parse(res);
            $('#' + direction + "_city").append('<option value="">Город не выбран</option>');
            $(res).each(function () {
                if(this.AOGUID == city) {
                    var selected = 'selected';
                } else {
                    var selected = null;
                }
                $('#' + direction + "_city").append('<option ' + selected + '  value="' + this.AOGUID + '">' + this.FORMALNAME + " " + this.SHORTNAME + '</option>');
                $('#' + direction + "_area").val(parent);
            });
        }
    }
});
